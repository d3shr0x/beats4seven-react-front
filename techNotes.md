## Beats4seven Frontend Technotes

### v0.8.0 10-08-2020
- Feed

### v0.7.6 06-08-2020
- Formularios de soporte no se enviaban si la persona no tenia nomnbre y appelido
- Los beats en raras ocasiones no cargan y generan un error en el frontend

### v0.7.5 05-08-2020
- Agregado redirección por defecto
- Mejorada la ultima pagina del registro cambiando el disco por una flecha bajando que se convierte en botón para entrar

### v0.7.0 02-08-2020
- Consentimiento de seguimiento RGDP

### v0.6.0 16-07-2020
- Formulario Soporte
- Mensaje de cabecera de "Cambio en marcha" cuando se cambia entre suscripciones

### v0.5.0 11-07-2020
- Reproductor ha vuelto a html5 audio api
- Formulario contacto portada
- Reproductor muestra modal diferente al comprar beats sin créditos

### v0.4.0 14-06-2020
- Añadida how it works
- imagenes de portada pasadas a webp

### v0.3.0 07-06-2020
- config ahora lee variables de entorno, si la version del servidor es mayor, recarga la página

### v0.2.0 19-02-2020
- Se ha implementado ErrorBoundary, lo cual captura un error de react, envia un reporte al backend y envia un informe por email