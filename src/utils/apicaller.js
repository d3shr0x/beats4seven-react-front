import axios from 'axios'
import config from '../config'
import * as types from '../actions/types'

export const apicaller = async ({url, method = 'get', data, headers, hasToken = false}, dispatch, getState) => {
  try {
    if (hasToken === true) {
      const {token, tokenExpires} = getState().user

      //Comprobar si el token ha expirado
      if (Math.round(new Date().getTime()/1000) > tokenExpires) {
        throw new Error()
      }

      //Añadir el token válido a la cabecera
      headers = {
        ...headers,
        common: {
          Authorization: `Bearer ${token}`
        }
      }


      //Comprobar si el token está a 24 horas de expirar
      //Y renovar si es así
      if ((tokenExpires - (Date.now()/1000)) < (24 * 3600)) {
        const newTokenResponse = await axios({
          method: 'post', 
          url: `${config.domain}/simple-jwt-authentication/v1/token/refresh`, 
          headers
        })

        if (newTokenResponse.data && newTokenResponse.data.token) {
          dispatch({
            type: types.REFRESH_TOKEN_SUCCESS,
            payload: {
              token: newTokenResponse.data.token,
              tokenExpires: newTokenResponse.data.token_expires
            }
          })
        }
      }
    }

    const response = await axios({method, url, data, headers})
    return response.data
  
  } catch (err) {
    if (err.response) {
      if (err.response.status === 403 && err.response.data && err.response.data.code === 'jwt_auth_invalid_token') {
        dispatch({type: types.EXPIRED_TOKEN})
      } else {
        throw (err.response)
      }
    }
  }
}
  