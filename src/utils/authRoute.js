import React from 'react'
import {Route, Redirect} from 'react-router'
import {connect} from 'react-redux'
import config from '../config'

const AuthRoute = ({
  component: Component,
  role,
  roles,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (config.roles[role] && roles.includes(role)) {
        return         <Component {...props} />
      } else if (config.roles[role]) {
        return (
          <Redirect
            to={{
              pathname: config.roles[role].defaultRoute,
              state: {from: props.location}
            }}
          />)
      } else {
        return (
          <Redirect
            to={{
              pathname: config.roles['guest'].defaultRoute,
              state: {from: props.location}
            }}
          />)
      }
    }}
  />
)

const mapStateToProps = ({user}) => ({
  role: user.role
})

export default connect(mapStateToProps)(AuthRoute)
