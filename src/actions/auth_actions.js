import * as types from "./types"
import {apicaller} from '../utils/apicaller'
import config from '../config'

export const userSignin = user => async (dispatch, getState) => {
  dispatch({type: types.USER_SIGNIN}) //Loading

  try {
    //login
    const login = await apicaller({
      url: `${config.domain}/b4s/v1/user/login`,
      method: 'post',
      data: user,
      hasToken: false
    }, dispatch, getState)

    //dispatch information
    dispatch({
      type: types.GET_USER_SUCCESS,
      payload: {
        userid: login.user_id,
        username: login.user_nicename,
        firstname: login.user_firstname,
        lastname: login.user_lastname,
        email: login.user_email,
        displayname: login.user_display_name,
        token: login.token,
        tokenExpires: login.tokenExpires,
        credits: login.credits,
        role: login.role,
        subscription: login.subscription,
        expiryDate: login.expiry_date
      }
    })

  } catch (err) {
    if (err.status === 403) {
      dispatch({type: types.USER_SIGNIN_FAIL, error: "Nombre de usuario o contraseña inválida"})
    } else {
      dispatch({type: types.USER_SIGNIN_FAIL, error: "Error desconocido, vuelve a intentarlo"})
    }
  }
}

export const userSignout = () => (dispatch, getState) => {
  dispatch({type: types.RESET_APP})
  dispatch({type: types.RESET_BEATS})
  dispatch({type: types.RESET_SIGNUP})
  dispatch({type: types.RESET_USER})
  dispatch({type: types.RESET_USER_BEATS})
  dispatch({type: types.RESET_USER_PROFILE})
  dispatch({type: types.RESET_USER_SUBSCRIPTION})
  apicaller({
    url: `${config.domain}/simple-jwt-authentication/v1/token/revoke`,
    method: 'post',
  }, dispatch, getState)
}

export const resetUser = () => (
  {type: types.RESET_USER_LOGIN}
)
