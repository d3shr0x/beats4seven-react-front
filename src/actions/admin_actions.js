import * as types from "./types"
import {apicaller} from '../utils/apicaller'
import config from '../config'


export const getAffiliateSubs = (i) => async (dispatch, getState) => {
  const {listaMeses} = getState().affiliate
  dispatch({type: types.GET_AFFILIATE_SUBS})
  const year = listaMeses[i].year
  const month = listaMeses[i].month
    
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/affiliate/subscriptions/${year}/${month}`,
      hasToken:true
    }, dispatch, getState)
  
    dispatch({type: types.GET_AFFILIATE_SUBS_SUCCESS, payload:data})
  } catch (err) {
    dispatch({type: types.GET_AFFILIATE_SUBS_FAIL})
  }
}