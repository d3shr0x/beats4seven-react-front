import {apicaller} from '../utils/apicaller'
import * as types from "./types"
import config from '../config'

export const createPaypalPayment = membership => async (dispatch, getState) => {
  dispatch({type:types.SET_USER_SUBSCRIPTION_PAYPAL})
  
  const isSub = getState().user.role === 'onboarding' ? false : true

  try {
    const formData = await apicaller({
      url: `${config.domain}/b4s/v1/user/subscription/paypal`,
      data: {membership},
      hasToken: true,
      method: 'post'
    },dispatch,getState)
    
    
    //Convertimos el objeto en un array
    const formArray = Object.keys(formData).map((k) => ({name: k, value: formData[k]}))

    //Si la suscripción cambiará, preparar el mensaje
    if (isSub) {
      dispatch({
        type: types.SET_USER_PENDING_MEMBERSHIP_CHANGE
      })
    }
    
  
    //Avisamos que el registro fue exitoso
    dispatch({
      type: types.SET_USER_SUBSCRIPTION_PAYPAL_SUCCESS,
      payload: [
        ...formArray,
        {name: 'return', value:  `${config.frontDomain}/sign-up/completed`},
        {name: 'cancel_return', value:  `${config.frontDomain}/sign-up/payment`},
      ]
    })
      
  } catch (err) {
    dispatch({
      type: types.SET_USER_SUBSCRIPTION_PAYPAL_FAIL,
      payload: 'Error desconocido, inténtalo de nuevo'
    })
  }
}
  
//Envia el token al backend junto a la informacion de suscripción
//El back-end nos avisa si el pago fue exitoso y nos vamos al panel de usuario
export const prepareStripePayment = data => async (dispatch, getState) => {
  dispatch({type:types.SET_USER_SUBSCRIPTION_STRIPE})

  const isSub = getState().user.role === 'onboarding' ? false : true
  
  try {
    const response = await apicaller({
      url: `${config.domain}/b4s/v1/user/subscription/stripe`,
      data:{
        product_id: data.membershipDetails.id,
        card_name: data.cardName,
        payment_method_id: data.pMethodId
      },
      hasToken: true,
      method: 'post'
    },dispatch,getState)
  
    if (response.requires_action === true) {
      //Avisamos que necesitamos autorizacion
      dispatch({
        type: types.SET_USER_SUBSCRIPTION_STRIPE_NEEDS_AUTHORIZATION,
        payload: {clientSecret: response.client_secret, transactionId: response.transaction_id}
      })
    } else if (response.success === true) {
      //Si la suscripción cambiará, preparar el mensaje
      if (isSub && isSub) {
        dispatch({
          type: types.SET_USER_PENDING_MEMBERSHIP_CHANGE
        })
      }
            
      //Avisamos que el registro fue exitoso
      dispatch({
        type: types.SET_USER_SUBSCRIPTION_STRIPE_SUCCESS
      })
    } else {
      throw Error()
    }
  } catch (err) {
    dispatch({
      type: types.SET_USER_SUBSCRIPTION_STRIPE_FAIL,
      payload: 'Error desconocido, inténtalo de nuevo'
    })
  }
}
  
export const createStripePayment = data => async (dispatch, getState) => {
  dispatch({type:types.SET_USER_SUBSCRIPTION_STRIPE})

  const isSub = getState().user.role === 'onboarding' ? false : true

  try {
    const response = await apicaller({
      url: `${config.domain}/b4s/v1/user/subscription/stripe`,
      data:{
        action: 'mepr_stripe_confirm_payment',
        transaction_id: data.transactionId,
        card_name: data.cardName,
        payment_intent_id: data.intentId
      },
      hasToken: true,
      method: 'post'
    },dispatch,getState)
  
    if (Number.isNaN(response.transaction_id)) {
      throw Error()
    } else {
      //Si la suscripción cambiará, preparar el mensaje
      if (isSub) {
        dispatch({
          type: types.SET_USER_PENDING_MEMBERSHIP_CHANGE
        })
      }

      //Avisamos que el registro fue exitoso
      dispatch({
        type: types.SET_USER_SUBSCRIPTION_STRIPE_SUCCESS
      })
    }
  
      
  } catch (err) {
    dispatch({
      type: types.SET_USER_SUBSCRIPTION_STRIPE_FAIL,
      payload: 'Error desconocido, inténtalo de nuevo'
    })
  }
}

export const resetUserSubscription = () => {
  return {
    type:types.RESET_USER_SUBSCRIPTION
  }
}
  