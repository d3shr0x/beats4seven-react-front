import * as types from "./types"
import {apicaller} from '../utils/apicaller'
import config from '../config'

export const getUserDetails = () => async (dispatch, getState) => {
  dispatch({type: types.GET_USER})
  
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/user`,
      hasToken:true
    }, dispatch, getState)

    //Borrar mensaje temporal si se detecta un cambio de suscripcion
    const currentSub = getState().user.subscription
    if (currentSub !== null && currentSub !== data.subscription) {
      dispatch({type: types.SET_USER_DISABLE_PENDING_MEMBERSHIP_CHANGE})
    }

    //dispatch information
    dispatch({
      type: types.GET_USER_SUCCESS,
      payload: {
        userid: data.user_id,
        username: data.user_nicename,
        firstname: data.user_firstname,
        lastname: data.user_lastname,
        email: data.user_email,
        displayname: data.user_display_name,
        token: data.token,
        tokenExpires: data.token_expires,
        credits: data.credits,
        role: data.role,
        subscription: data.subscription,
        expiryDate: data.expiry_date
      }
    })
  } catch (err) {
    //Si no se puede recibir la información del usuario, recarga la página
    //dispatch({type: types.OUTDATED_FRONT_END})
  }
}

export const getUserCredits = () => async (dispatch, getState) => {
  dispatch({type: types.GET_USER_CREDITS})
  
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/user/credits`,
      hasToken:true
    }, dispatch, getState)

    setTimeout(() => dispatch({type: types.GET_USER_CREDITS_SUCCESS, payload:data}), 3000)
  } catch (err) {
    console.log(3100, err)
  }
}

export const getUserFeed = () => async (dispatch, getState) => {
  dispatch({type: types.GET_USER_FEED})
  
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/user/feed`,
      hasToken:true
    }, dispatch, getState)

    dispatch({type: types.GET_USER_FEED_SUCCESS, payload:data})
  } catch (err) {
    dispatch({type: types.GET_USER_FEED_FAIL})
  }
}

export const setLikeUserFeed = feedid => (dispatch, getState) => {  
  try {
    apicaller({
      url: `${config.domain}/b4s/v1/user/feed/like`,
      method: 'post',
      data: {feedid},
      hasToken:true
    }, dispatch, getState)

    dispatch({type: types.SET_USER_FEED_LIKE_SUCCESS, payload:{feedid}})
  } catch (err) {
    console.log(3019)
  }
}

export const getUserBeats = () => async (dispatch, getState) => {
  dispatch({type: types.GET_USER_BEATS})
  
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/user/beats`,
      hasToken:true
    }, dispatch, getState)

    if (Array.isArray(data)) {
      dispatch({type: types.GET_USER_BEATS_SUCCESS, payload:data})
    } else {
      throw Error()
    }
  } catch (err) {
    dispatch({type: types.GET_USER_BEATS_FAIL})
  }
}

export const setFavouriteUserBeat = beatid => (dispatch, getState) => {  
  try {
    apicaller({
      url: `${config.domain}/b4s/v1/user/beat/favourite`,
      method: 'post',
      data: {beatid},
      hasToken:true
    }, dispatch, getState)

    dispatch({type: types.SET_USER_BEAT_FAVOURITE_SUCCESS, payload:{beatid}})
  } catch (err) {
  }
}

export const purchaseUserBeat = beatid => async (dispatch, getState) => {  
  try {
    dispatch({type: types.LOAD_USER_BEAT, payload: {beatid}})
    await apicaller({
      url: `${config.domain}/b4s/v1/user/beat/purchase`,
      method: 'post',
      data: {beatid},
      hasToken:true
    }, dispatch, getState)
    dispatch({type: types.SET_USER_BEAT_PURCHASE_SUCCESS, payload:{beatid}})
  } catch (err) {
    dispatch({type: types.SET_USER_BEAT_PURCHASE_FAIL, payload:{beatid}})
  }
}

export const upgradeUserBeat = beatid => async (dispatch, getState) => {
  dispatch({type: types.LOAD_USER_BEAT_UPGRADE, payload: {beatid}})  
  try {
    await apicaller({
      url: `${config.domain}/b4s/v1/user/beat/upgrade`,
      method: 'post',
      data: {beatid},
      hasToken:true
    }, dispatch, getState)

    dispatch({type: types.LOAD_USER_BEAT_UPGRADE_SUCCESS, payload: {beatid}})
  } catch (err) {
    dispatch({type: types.LOAD_USER_BEAT_UPGRADE_FAIL, payload: {beatid}})
  }
}

export const downloadUserBeat = (beatid) => async (dispatch, getState) => {
  dispatch({type: types.LOAD_USER_BEAT, payload: {beatid}})

  try {
    const downloadUrl = await apicaller({
      url: `${config.domain}/b4s/v1/user/beat/download`,
      method: 'post',
      data: {beatid},
      hasToken:true
    }, dispatch, getState)

    //Esto obliga al navegador a abrirlo, como es un archivo, lo trata como descarga
    window.location = downloadUrl

    dispatch({type: types.LOAD_USER_BEAT_SUCCESS, payload: {beatid}})
  } catch (err) {
    dispatch({type: types.LOAD_USER_BEAT_FAIL})
  }
}

export const getUserSubscription = () => async (dispatch, getState) => {
  dispatch({type: types.GET_USER_SUBSCRIPTION})
  
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/user/subscription`,
      hasToken:true
    }, dispatch, getState)

    //Borrar mensaje temporal si se detecta un cambio de suscripcion
    const currentMeth = getState().userSubscription.paymentMethod
    if (currentMeth !== data.paymentMethod) {
      dispatch({type: types.SET_USER_DISABLE_PENDING_MEMBERSHIP_CHANGE})
    }

    dispatch({type: types.GET_USER_SUBSCRIPTION_SUCCESS, payload:data})
  } catch (err) {
    dispatch({type: types.GET_USER_SUBSCRIPTION_FAIL})
  }
}

export const chooseUserSubscription = (membership) => ({
  type: types.CHOOSE_USER_SUBSCRIPTION,
  payload:  membership
})

export const cancelUserSubscription = () => async (dispatch, getState) => {
  dispatch({type: types.CANCEL_USER_SUBSCRIPTION})
  
  try {
    await apicaller({
      url: `${config.domain}/b4s/v1/user/subscription/cancel`,
      method: 'post',
      hasToken:true
    }, dispatch, getState)

    dispatch({type: types.CANCEL_USER_SUBSCRIPTION_SUCCESS})
  } catch (err) {
    dispatch({type: types.CANCEL_USER_SUBSCRIPTION_FAIL})
  }
}

export const setUserProfile = data => async (dispatch, getState) => {
  dispatch({type: types.SET_USER_PROFILE})
  
  try {
    await apicaller({
      url: `${config.domain}/b4s/v1/user/profile`,
      method: 'put',
      data,
      hasToken:true
    }, dispatch, getState)

    dispatch({type: types.SET_USER_PROFILE_SUCCESS})
    dispatch({type: types.USER_UPDATE_SUCCESS, payload: data})
  } catch (err) {
    dispatch({type: types.SET_USER_PROFILE_FAIL, payload: err.data})
  }
}

export const setUserPassword = data => async (dispatch, getState) => {
  dispatch({type: types.SET_USER_PROFILE_PASSWORD})
  
  try {
    await apicaller({
      url: `${config.domain}/b4s/v1/user/profile/password`,
      method: 'put',
      data,
      hasToken:true
    }, dispatch, getState)

    dispatch({type: types.SET_USER_PROFILE_PASSWORD_SUCCESS})
  } catch (err) {
    dispatch({type: types.SET_USER_PROFILE_PASSWORD_FAIL, payload: err.data})
  }
}
