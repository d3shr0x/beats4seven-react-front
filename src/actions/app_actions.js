import * as types from "./types"
import {apicaller} from '../utils/apicaller'
import config from '../config'

export const healthCheck = () => async (dispatch, getState) => {
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/healthcheck`
    }, dispatch, getState)

    dispatch({type: types.SET_APP_VERSION, payload: data.frontVersion})

  } catch (err) {
    dispatch({type: types.HEALTHCHECK_FAIL})
  }
}

export const sendErrorReport = (error, errorInfo) => async (dispatch, getState) => {
  const hasToken = getState().user.tokenExpires ? true : false
  const store = btoa(JSON.stringify(getState()))
  await apicaller({
    url: `${config.domain}/b4s/v1/error/report`,
    data:{
      error,
      errorInfo,
      store,
      appVersion: config.appVersion
    },
    method: 'post',
    hasToken
  }, dispatch, getState).catch(err => {})

  dispatch(clearStore())
}

export const clearStore = () => dispatch => {
  dispatch({type: types.RESET_APP})
  dispatch({type: types.RESET_BEATS})
  dispatch({type: types.RESET_SIGNUP})
  dispatch({type: types.RESET_USER})
  dispatch({type: types.RESET_USER_BEATS})
  dispatch({type: types.RESET_USER_PROFILE})
  dispatch({type: types.RESET_USER_SUBSCRIPTION})
}