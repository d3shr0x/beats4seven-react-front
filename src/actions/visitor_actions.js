import * as types from "./types"

export const giveConsent = () => ({type: types.COOKIE_CONSENT_TRUE})

export const denyConsent = () => ({type: types.COOKIE_CONSENT_FALSE})
