import * as types from "./types"
import {apicaller} from '../utils/apicaller'
import config from '../config'

export const getBeats = () => async (dispatch, getState) => {
  dispatch({type: types.GET_BEATS})

  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/beats`
    }, dispatch, getState)

    //We need to check if the response from back-end is valid or not
    if (Array.isArray(data)) {
      dispatch({type: types.GET_BEATS_SUCCESS, payload:data})
    } else {
      throw Error()
    }
  } catch (err) {
    dispatch({type: types.GET_BEATS_FAIL})
  }
}

export const resetPassword = (username) => async (dispatch, getState) => {
  dispatch({type: types.RESET_PASSWORD})

  try {
    const data = await apicaller({
      url: `${config.domain}/simple-jwt-authentication/v1/token/resetpassword`,
      method: 'post',
      hasToken: false,
      data: {
        username
      }
    }, dispatch, getState)

    if (data.code === 'jwt_auth_invalid_username') {
      dispatch({type: types.RESET_PASSWORD_FAIL, error: 'Usuario o correo electrónico inválido'})
    } else {
      dispatch({type: types.RESET_PASSWORD_SUCCESS})
    }

  } catch (err) {
    dispatch({type: types.RESET_PASSWORD_FAIL, error: 'Usuario o correo electrónico inválido'})
  }
}