import ReactPixel from 'react-facebook-pixel'
import {apicaller} from '../utils/apicaller'
import * as types from "./types"
import config from '../config'

export const setSignupAffid = affid => {
  return {
    type: types.SIGNUP_SET_AFFILIATE,
    payload: affid
  }
}

export const setSignupSub = sub => {
  ReactPixel.track('Lead', {content_name: sub}) 
  return {
    type: types.SIGNUP_SET_MEMBERSHIP,
    payload: sub
  }
}

export const setSignupPersonal = details => async (dispatch, getState) => {
  dispatch({type:types.SIGNUP_LOAD_REGISTRATION})
  try {
    const login = await apicaller({
      url: `${config.domain}/b4s/v1/user`,
      data: details,
      hasToken: false,
      method: 'post'
    },dispatch,getState)

    //Recibimos la información del usuario nuevo creado
    //Y lo conectamos
    dispatch({
      type: "GET_USER_SUCCESS",
      payload: {
        userid: login.userid,
        username: login.user_nicename,
        email: login.user_email,
        displayname: login.user_display_name,
        token: login.token,
        tokenExpires: login.tokenExpires,
        credits: 0,
        role: 'onboarding',
        expiryDate: '0000-00-00 00:00:00'
      }
    })

    //Avisamos que el registro fue exitoso
    dispatch({
      type: types.SIGNUP_LOAD_REGISTRATION_SUCCESS
    })

    ReactPixel.track('CompleteRegistration')
    
  } catch (err) {
    if (err && err.status === 400) {
      dispatch({
        type: types.SIGNUP_LOAD_REGISTRATION_FAIL,
        payload: err.data
      })
    } else {
      dispatch({
        type: types.SIGNUP_LOAD_REGISTRATION_FAIL,
        payload: 'Error desconocido, inténtalo de nuevo'
      })
    }
  }
}

export const verifyOnboard = () => async (dispatch, getState) => {
  dispatch({type: types.GET_USER})
  
  try {
    const data = await apicaller({
      url: `${config.domain}/b4s/v1/user`,
      hasToken:true
    }, dispatch, getState)

    //dispatch information
    dispatch({
      type: types.GET_USER_SUCCESS,
      payload: {
        userid: data.user_id,
        username: data.user_nicename,
        firstname: data.user_firstname,
        lastname: data.user_lastname,
        email: data.user_email,
        displayname: data.user_display_name,
        token: data.token,
        tokenExpires: data.token_expires,
        credits: data.credits,
        role: data.role,
        subscription: data.subscription,
        expiryDate: data.expiry_date
      }
    })

    setTimeout(() => dispatch({type: types.SIGNUP_VERIFY_SUBSCRIPTION, payload:data}), 2000)
  } catch (err) {
  }
}
