import * as types from "../actions/types"

const initialState = {
  error: null,
  cancelError: null,
  isLoading: null,
  isLoadingCancel: null,
  membership: null,
  paymentMethod: null,
  isCancelled: false,
  paymentAddress: null,
  formData: [],
  transactionId: null,
  isPaypalLoading: false,
  isStripeLoading: false,
  isStripeProcessed: false,
  stripeClientSecret: null,
  subChange: false
}

const userSubscriptionReducer = (state = initialState, action) => {
  switch (action.type) {
  case types.GET_USER_SUBSCRIPTION:
    return  {...initialState, isLoading: true}
  case types.GET_USER_SUBSCRIPTION_SUCCESS:
    return  {
      ...initialState, 
      paymentMethod: action.payload.paymentMethod,
      paymentAddress: action.payload.paymentAddress,
      isCancelled: action.payload.isCancelled
    }
  case types.CHOOSE_USER_SUBSCRIPTION:
    return {...state, membership: action.payload}
  case types.CANCEL_USER_SUBSCRIPTION:
    return  {...state, isLoadingCancel: true}
  case types.CANCEL_USER_SUBSCRIPTION_FAIL:
    return  {...state, cancelError: true, isLoadingCancel: false}
  case types.CANCEL_USER_SUBSCRIPTION_SUCCESS:
    return  {...state, isCancelled: true, isLoadingCancel: false}
  case types.SET_USER_SUBSCRIPTION_PAYPAL:
    return  {...state, isPaypalLoading: true, error: false}
  case types.SET_USER_SUBSCRIPTION_PAYPAL_SUCCESS:
    return  {...state, isPaypalLoading: false, error: false, formData: action.payload}
  case types.SET_USER_SUBSCRIPTION_PAYPAL_FAIL:
    return  {...state, isPaypalLoading: false, error: action.payload, formData: []}
  case types.SET_USER_SUBSCRIPTION_STRIPE:
    return  {...state, isStripeLoading: true, error: false}
  case types.SET_USER_SUBSCRIPTION_STRIPE_NEEDS_AUTHORIZATION:
    return  {
      ...state, 
      isStripeLoading: false, 
      error: false, 
      isStripeProcessed: false, 
      stripeClientSecret: action.payload.clientSecret, 
      transactionId: action.payload.transactionId
    }
  case types.SET_USER_SUBSCRIPTION_STRIPE_SUCCESS:
    return  {...state, isStripeLoading: false, error: false, isStripeProcessed: true, stripeClientSecret: null}
  case types.SET_USER_SUBSCRIPTION_STRIPE_FAIL:
    return  {...state, isStripeLoading: false, error: action.payload, stripeClientSecret: null}
    
  case types.RESET_USER_SUBSCRIPTION:
    return  {...initialState}
  default: 
    return state
  }
}

export default userSubscriptionReducer
