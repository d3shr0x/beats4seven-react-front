import * as types from "../actions/types"

const initialState = {
  userid: null,
  username: "",
  firstname: "",
  lastname: "",
  email: "",
  displayname: "",
  token: null,
  tokenExpires: null,
  credits: 0,
  role: "guest",
  subscription: null,
  expiryDate: "",
  creditCounter: 0,
  subChangeInProgress: false
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
  case types.GET_USER_SUCCESS:
    
    return {
      ...state,
      isLoading: false,
      userid: action.payload.userid,
      username: action.payload.username,
      firstname: action.payload.firstname,
      lastname: action.payload.lastname,
      token: action.payload.token,
      tokenExpires: action.payload.tokenExpires,
      email: action.payload.email,
      displayname: action.payload.displayname,
      credits: action.payload.credits,
      role: action.payload.role,
      subscription: action.payload.subscription,
      expiryDate: action.payload.expiryDate,
    }
  case types.USER_UPDATE_SUCCESS: 
    return {
      ...state,
      firstname: action.payload.firstname,
      lastname: action.payload.lastname,
      email: action.payload.email,
      displayname: action.payload.firstname+' '+action.payload.lastname,
    }
  case types.REFRESH_TOKEN_SUCCESS:
    return {
      ...state,
      token: action.payload.token,
      tokenExpires: action.payload.tokenExpires
    }
  case types.SET_USER_BEAT_PURCHASE_SUCCESS:
    return {
      ...state,
      credits: state.credits-1
    }
  case types.GET_USER_CREDITS_SUCCESS:
    return {
      ...state,
      credits: action.payload,
      creditCounter: state.creditCounter ? 0 : 1
    }
  case types.SET_USER_PENDING_MEMBERSHIP_CHANGE:
    return {
      ...state,
      subChangeInProgress: true
    }
  case types.SET_USER_DISABLE_PENDING_MEMBERSHIP_CHANGE:
    return {
      ...state,
      subChangeInProgress: false
    }
  case types.RESET_USER:
    return  {...initialState}
  default: 
    return state
  }
}

export default userReducer