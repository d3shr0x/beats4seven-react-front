import * as types from "../actions/types"

const initialState = {
  passwordResetSuccess: false,
  error: null,
  loading: false,
}

const userLoginReducer = (state = initialState, action) => {
  switch (action.type) {
  case types.RESET_PASSWORD:
  case types.USER_SIGNIN:
    return  {...initialState, loading: true}
  case types.RESET_PASSWORD_FAIL:
  case types.USER_SIGNIN_FAIL:
    return  {...initialState, error: action.error}
  case types.RESET_PASSWORD_SUCCESS:
    return  {...initialState, passwordResetSuccess: true}
  case types.RESET_USER_LOGIN:
    return  {...initialState}
  default: 
    return state
  }
}

export default userLoginReducer