import * as types from "../actions/types"

const initialState = {
  cookieConsent: null
}

const appReducer = (state = initialState, action) => {
  switch (action.type) {
  case types.COOKIE_CONSENT_TRUE:
    return  {...initialState, cookieConsent: true}
  case types.COOKIE_CONSENT_FALSE:
    return  {...initialState, cookieConsent: false}
  case types.RESET_VISITOR:
    return  {...initialState}
  default: 
    return state
  }
}

export default appReducer