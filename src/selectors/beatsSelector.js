import {createSelector} from 'reselect'
import config from '../config'

const getBeats = beats => beats
const getFilter = (state, filter) => filter
const oldTime = ((Date.now()/1000)-config.newBeatTime)

const beatsSelector = createSelector(
  getFilter, getBeats,
  (filter, beats) => {
    switch (filter) {
    case 'NEW':
      return beats.filter(b => b.publishdate > oldTime)
    case 'USER':
      return beats.filter(b => b.purchased === true)
    case 'FAVOURITE':
      return beats.filter(b => b.favourite === true)
    case 'CAT1':
      return beats.filter(b => parseInt(b.category) === 1)
    case 'CAT2':
      return beats.filter(b => parseInt(b.category) === 2)
    case 'CAT3':
      return beats.filter(b => parseInt(b.category) === 3)
    case 'CAT4':
      return beats.filter(b => parseInt(b.category) === 4)
    case 'CAT5':
      return beats.filter(b => parseInt(b.category) === 5)
    case 'CAT6':
      return beats.filter(b => parseInt(b.category) === 6)
    default:
      return beats
    }
  }
)

export default beatsSelector
