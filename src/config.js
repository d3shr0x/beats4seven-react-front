import pjson from '../package.json'

const config = {
  "domain": process.env.REACT_APP_BACKEND_URL || "https://api.discocanarias.com/wp-json",
  "frontDomain": process.env.REACT_APP_FRONTEND_URL || "https://beats4seven.web.app",
  "appVersion": pjson.version || "0.1.0",
  "s3mp3dir": process.env.REACT_APP_S3_MP3_DIR || "https://s3.amazonaws.com/beats4seven/listen/",
  "paypalGateway": process.env.REACT_APP_PAYPAL_GW || "https://www.sandbox.paypal.com/cgi-bin/webscr",
  "newBeatTime" : 2592000,
  "stripeKey": process.env.REACT_APP_STRIPE_KEY || "pk_test_XfirumCKLUbo7T4JnsBuFsvT008VCZVLyx",
  "facebookPixel": process.env.REACT_APP_FB_PIXEL || "541959786683258",
  "hotjarId": process.env.REACT_APP_HOTJAR_ID || "929504",
  "memberships": [
    {
      "id": process.env.REACT_APP_SUB_STD_ID || "6322",
      "name": "standard",
      "price": process.env.REACT_APP_SUB_STD_PRICE || "7",
      "details": {
        "name": "Membresia Standard",
        "service": "4 Beats cada mes",
        "cycle": "7$ en pagos mensuales"
      },
    },
    {
      "id": process.env.REACT_APP_SUB_VIP_ID || "6342",
      "name":"vip",
      "price": process.env.REACT_APP_SUB_VIP_PRICE || "12",
      "details": {
        "name": "Membresia VIP",
        "service": "4 Beats cada mes",
        "cycle": "12$ en pagos mensuales"
      },
    },
    {
      "id": process.env.REACT_APP_SUB_VIP6_ID || "6483",
      "name":"vip6",
      "price": process.env.REACT_APP_SUB_VIP6_PRICE || "54",
      "details": {
        "name": "Membresia VIP",
        "service": "4 Beats cada mes",
        "cycle": "54$ en pagos semestrales"
      },
    }
  ],
  "roles": {
    "guest":{
      "defaultRoute": "/"
    },
    "onboarding": {
      "defaultRoute": "/sign-up/payment"
    },
    "standard":{
      "defaultRoute": "/user-area"
    },
    "vip":{
      "defaultRoute": "/user-area"
    },
    "expired":{
      "defaultRoute": "/user-area"
    },
    "affiliate":{
      "defaultRoute": "/affiliate-area"
    }
  },
  "framer": {
    "pageVariant": {
      "initial": {
        "x": -200,
        "opacity": 0
      },
      "animate": {
        "x": 0,
        "opacity": 1,
        "transition": {"duration": 0.5}
      },
      "exit": {
        "x": 200,
        "opacity": 0,
        "transition": {"duration": 0.5}
      }
    }
  },
  "forms": {
    "contacto": process.env.REACT_APP_FORM_CONTACT || "6881",
    "soporte": process.env.REACT_APP_FORM_SUPPORT || "6882"
  }
}

export default config
