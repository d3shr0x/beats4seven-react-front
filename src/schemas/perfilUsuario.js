import * as yup from 'yup'

/**
 * Esquema para el formulario del registro del usuario
 * */

export const userProfileScheme = yup.object().shape({
  firstname: yup
    .string()
    .required('Nombre es un campo requerido')
    .min(2, 'Su nombre es demasiado corto')
    .max(250, 'Su nombre excede el máximo permitido'),
  lastname: yup
    .string()
    .required('Apellidos es un campo requerido')
    .min(3, 'Su apellido es demasiado corto')
    .max(250, 'Su apellido excede el máximo permitido'),
  email: yup
    .string()
    .required('Correo electrónico es un campo requerido')
    .email('No se ha insertado un correo electrónico válido'),
})

export const passFormScheme = yup.object().shape({
  oldpass: yup
    .string()
    .required('Contraseña actual es un campo requerido')
    .min(6, 'Su contraseña debe tener al menos 6 caracteres')
    .max(50, 'Su contraseña no debe tener más de 50 caracteres'),
  newpass: yup
    .string()
    .required('Contraseña nueva es un campo requerido')
    .min(6, 'Su contraseña debe tener al menos 6 caracteres')
    .max(50, 'Su contraseña no debe tener más de 50 caracteres'),
  repeatpass: yup
    .string()
    .required('Repetir contraseña es un campo requerido')
    .oneOf([yup.ref('newpass'), null], 'Las contraseñas no coinciden')
})