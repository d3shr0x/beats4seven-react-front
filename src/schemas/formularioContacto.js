import * as yup from 'yup'

/**
 * Esquema para el formulario del contacto
 * */

export const formularioContacto = yup.object().shape({
  name: yup
    .string()
    .required('Nombre es un campo requerido')
    .min(2, 'Su nombre es demasiado corto')
    .max(250, 'Su nombre excede el máximo permitido'),
  email: yup
    .string()
    .required('Correo electrónico es un campo requerido')
    .email('No se ha insertado un correo electrónico válido'),
  message: yup
    .string()
    .required('Mensaje es un campo requerido')
    .min(3, 'Tu mensaje es demasiado corto')
    .max(250, 'Tu mensaje excede el máximo permitido'),
})