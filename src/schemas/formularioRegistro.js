import * as yup from 'yup'

/**
 * Esquema para el formulario del registro del usuario
 * */

export const formularioRegistro = yup.object().shape({
  firstname: yup
    .string()
    .required('Nombre es un campo requerido')
    .min(2, 'Su nombre es demasiado corto')
    .max(250, 'Su nombre excede el máximo permitido'),
  lastname: yup
    .string()
    .required('Apellidos es un campo requerido')
    .min(3, 'Su apellido es demasiado corto')
    .max(250, 'Su apellido excede el máximo permitido'),
  username: yup
    .string()
    .required('Nombre de usuario es un campo requerido')
    .matches(/^[a-z0-9]+$/i, 'Nombre de usuario solo debe contener letras y números')
    .min(3, 'El nombre de usuario debe de tener al menos 3 caracteres')
    .max(30, 'El nombre de usuario debe de tener máximo 30 caracteres'),
  email: yup
    .string()
    .required('Correo electrónico es un campo requerido')
    .email('No se ha insertado un correo electrónico válido'),
  password: yup
    .string()
    .required('Contraseña es un campo requerido')
    .min(6, 'Su contraseña debe tener al menos 6 caracteres')
    .max(50, 'Su contraseña no debe tener más de 50 caracteres'),
  repeat_password: yup
    .string()
    .required('Repetir contraseña es un campo requerido')
    .oneOf([yup.ref('password'), null], 'Las contraseñas no coinciden')
})