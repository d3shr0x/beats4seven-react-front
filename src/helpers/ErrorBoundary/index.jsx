import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {sendErrorReport} from '../../actions/app_actions'
import Footer from '../../components/Footer'
import styles from './ErrorBoundary.module.scss'
import Button from '../../components/Button'
import Header from '../../components/Header'


class ErrorBoundary extends React.Component {
    state = {hasError: false}

    static getDerivedStateFromError(error) {
      // Update state so the next render will show the fallback UI.
      return {hasError: true}
    }
  
    componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
      this.props.sendErrorReport(error.toString(), btoa(JSON.stringify(errorInfo.componentStack)))
    }

    handleError() {
      this.props.history.push('/')
      this.setState({hasError: false})
    }

  
    render() {
      if (this.state.hasError) {
        const errorImg = require('../../assets/img/error-resource.svg')

        return (
          <React.Fragment>
            <div className={styles.background}>
              <div className={styles.container}>
                <Header />
                <div className={styles.content}>
                  <div className={styles.left}>
                    <h2 className={styles.h2}>Ha ocurrido un error</h2>
                    <p className={styles.p}>¡Lo sentimos!, Nuestros ingenieros han recibido un informe y ya se han puesto en marcha para resolver el problema.</p>
                    <Button onClick={() => this.handleError()} className={styles.button} dark>Vuelve a la portada</Button>
                  </div>
                  <div className={styles.right}>
                    <img className={styles.img} src={errorImg} alt="An Error has happened" />
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </React.Fragment>
        )
      }
  
      return this.props.children 
    }
}

export default withRouter(connect(null, {sendErrorReport})(ErrorBoundary))
