// import './utils/wdyr'
import 'core-js'
import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {BrowserRouter} from 'react-router-dom'
import {IntlProvider} from 'react-intl'
import {PersistGate} from 'redux-persist/es/integration/react'

import text_en from './locale/en.json'
import text_es from './locale/es.json'
import App from './pages/App'
import configureStore from './reducers'
import ScrollToTop from './helpers/ScrollToTop'

//Application css import
import './assets/styles/app.scss'
import ErrorBoundary from './helpers/ErrorBoundary'

const usersLocale = navigator.language.split(/[-_]/)[0]
const messages = usersLocale.includes('en') ? text_en : text_es

const {persistor, store} = configureStore()

ReactDOM.render(
  <Provider  store={store}>
    <IntlProvider messages={messages} locale={usersLocale} key={usersLocale} defaultLocale='es'>
      <PersistGate  persistor={persistor}>
        <BrowserRouter>
          <ScrollToTop />
          <ErrorBoundary>
            <App />
          </ErrorBoundary>
        </BrowserRouter>
      </PersistGate>
    </IntlProvider>
  </Provider>,
  document.getElementById('root')
)
