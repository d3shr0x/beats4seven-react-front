import React, {useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import Button from '../Button'
import styles from './CookieConsent.module.scss'
import {giveConsent, denyConsent} from '../../actions/visitor_actions'

const CookieConsent = () => {
  const [moreInfo, wantMoreInfo] = useState(false)
  const dispatch = useDispatch()


  const {cookieConsent} = useSelector(state => (
    {
      cookieConsent: state.visitor.cookieConsent
    }
  ))

  //Si ya tenemos una decisión, no mostrar más
  if (typeof cookieConsent === 'boolean') {
    return ''
  }

  const firstLayer = () => (
    <div className={styles.cookies}>
      <div className={styles.container}>
        <div className={styles.title}>
          <h3>Política de RGDP</h3>
        </div>
        <div className={styles.twoRows}>
          <div className={styles.content}>
            <p>Queremos ofrecerte gran contenido, pero necesitamos saber como usas la página para poder mejorarla.  <br/> <br/>
        Si nos autorizas a seguir tus movimientos, tenemos acceso a analíticas que nos ayudan a entender que áreas de la página pueden causar confusión. <br/> <br/>
        Por supuesto, siempre podrás enviarnos tu feedback por el formulario de contacto, estamos encantados de recibir tu opinión sobre nuestra página.
            </p>
          </div>
          <div className={styles.actions}>
            <Button onClick={() => dispatch(giveConsent())}>Acepto</Button>
            <Button dark onClick={() => wantMoreInfo(true)}>Más Información</Button>
          </div>
        </div>
      </div>
    </div>
  )

  const secondLayer = () => (
    <div className={styles.cookies}>
      <div className={styles.container}>
        <div className={styles.title}>
          <h3>Política de RGDP</h3>
        </div>
        <div className={styles.twoRows}>
          <div className={styles.content}>
            <h4>Pixel de Facebook</h4>
            <p>
            El pixel de facebook nos ayuda a seguir movimientos de visitantes, seguiremos específicamente los pasos del registro, para sacar conclusiones sobre los visitantes que deciden no suscribirse durante el proceso del registro, con el fin de mejorar nuestro proceso. <br /> <br />
            El pixel también nos ayudará en limitadas ocasiones a recopilar datos del visitante con el único fin de mostrar nuestra publicidad a visitantes con intereses similares a los que se han suscrito a la página. No nos será posible identificar personas individuales que visitan nuestra página.
            </p>
        
            <h4>Hotjar</h4>
            <p>
            Hotjar es un servicio que graba sesiones de visitante, siguiendo los movimientos del ratón y el scroll. Esto ocurre de forma anónima: Tendremos acceso a tu resolución y versión de navegador, tu sistema operativo, pero no tu IP. Vemos tus movimientos, pero no podemos ver los campos de texto, por lo que nos es imposible ver los datos de tu tarjeta de crédito, por ejemplo. <br /> <br />
            Nosotros usamos hotjar para ver comportamientos de usuarios, por ejemplo si nadie navega hacia el final de la portada, podemos sacar la conclusión de que tenemos que hacer una portada más pequeña. Solo usamos los datos para mejorar la experiencia en la página e identificar que áreas de la página necesitan mejora.
            </p>
          </div>
          <div className={styles.actions}>
            <Button  onClick={() => dispatch(giveConsent())}>Dar consentimiento</Button>
            <Button dark  onClick={() => dispatch(denyConsent())}>No dar consentimiento</Button>
          </div>
        </div>
      </div>
    </div>
  )
  
  return (
    moreInfo === false ? firstLayer() : secondLayer()
  )

}


export default CookieConsent
