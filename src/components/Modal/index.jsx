import React, {Component} from 'react'
import styles from './Modal.module.scss'

class Modal extends Component {

  handleEsc = (e) =>{
    if (e.keyCode === 27) {
      this.props.onClick()
    }
  }


  render() {
    let classes = `${styles.overlay} ${this.props.className}`

    return (
      <div 
        className={classes}
        id={this.props.id}
        onClick={() => this.props.onClick()}
        onKeyPress={() => this.handleEsc()}
        role="presentation"
      >
        {this.props.children}
      </div>
    )
  }
}


export default Modal