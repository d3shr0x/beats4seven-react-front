import React, {Component} from 'react'
import styles from './Overlay.module.scss'

class Overlay extends Component {

  render() {
    let classes = `${styles.overlay} ${this.props.className}`

    return (
      <div 
        className={classes}
        id={this.props.id}
        onClick={() => this.props.onClick()}
        role="presentation"
      >
        {this.props.children}
      </div>
    )
  }
}


export default Overlay