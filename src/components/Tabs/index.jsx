import React, {Component} from 'react'
import styles from './Tabs.module.scss'

export class Tabs extends Component {

  render() {
    return (
      <div {...this.props} className={styles.tabs}>
        {this.props.children.map((tab) => (
          //We are injecting the active tab id into the tab
          {...tab, props: {...tab.props, active:this.props.active}}
        ))}
      </div>
    )
  }
}

export class Tab extends Component {
  render() {
    const {active, index} = this.props
    let isActiveTab = false
    if (parseInt(active) === parseInt(index)) {
      isActiveTab = true
    }

    return (
      <div {...this.props} className={`${styles.tab} ${isActiveTab && styles.active}`}>
        <span>{this.props.children}</span>
      </div>    
    )
  }
}
