import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import styles from './Footer.module.scss'

class Footer extends Component {

  render() {
    const b4sLogo = require('../../assets/img/logo-footer.svg')


    return (
      <div className={styles.footer}>
        <div className={styles.container}>
          <div className={styles.firstRow}>
            {/*
            <div className={styles.lang}>
              <i className={`fas fa-language ${styles.langIcon}`}></i>
              <i className={`far fa-angle-down ${styles.arrowIcon}`}></i>
              <select>
                <option>Español</option>
                <option>English</option>
              </select>
            </div>
            */}
            <div className={styles.logo}>
              <img alt="logo" src={b4sLogo} />
            </div>
            <div className={styles.social}>
              <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/beats4seven/" ><i className={`fab fa-instagram ${styles.icon}`}></i></a>
              <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/beats4seven/" ><i className={`fab fa-facebook-square ${styles.icon}`}></i></a>
            </div>
          </div>
          <hr className={styles.hr} />
          <div className={styles.secondRow}>
            <nav>
              <ul className={styles.menu}>
                <li className={styles.menuLink}><Link to="/terminos-condiciones"><span>Términos y condiciones</span></Link></li>
                <li className={styles.menuLink}><Link to="/politica-privacidad"><span>Política de privacidad</span></Link></li>
                <li className={styles.menuLink}><Link to="/aviso-legal"><span>Aviso legal</span></Link></li>
                <li className={styles.menuLink}><Link to="/faq"><span>FAQ</span></Link></li>
                <li className={styles.menuLink}><Link to="/politica-cookies"><span>Política de cookies</span></Link></li>
                <li className={styles.menuLink}><Link to="/how-it-works"><span>Como funciona</span></Link></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    )
  }
}


export default Footer
