import React, {useState} from 'react'
import config from '../../config'
import Button from '../../components/Button'
import styles from './Subscriptiontable.module.scss'
import Card from '../Card'
import Overlay from '../Overlay'

const Subscriptiontable = props => {

  const [vipModal, setVipModal] = useState(false)


  const b4sTypo = require('../../assets/img/logo-typo.svg')
  const check = require('../../assets/img/check.svg')
  const line = require('../../assets/img/line.svg')


  const renderModal = () => {
    if (vipModal === true) {
      return (
        <Overlay className={styles.overlay} onClick={() => setVipModal(!vipModal)}>
          <Card className={styles.vipModal}>
            <div className={styles.head}>
              <p className={styles.title}>Elige tu plan</p>
            </div>
            <div className={styles.body}>
              <p className={styles.p} >El plan VIP te da acceso a la experiencia completa de Beats4seven. Elige a continuación la duración de tu suscripción.</p>
              <div className={styles.plans}>
                <div className={styles.plan} onClick={() => props.buttonAction('vip')}>
                  <h3 className={styles.vip}>VIP</h3>
                  <h4 className={styles.period}>MENSUAL</h4>
                  <p className={styles.price}>Por {config.memberships.find(m => m.name === 'vip').price}$ al mes</p>
                </div>
                <div className={styles.plan} onClick={() => props.buttonAction('vip6')}>
                  <h3 className={styles.vip}>VIP</h3>
                  <h4 className={styles.period}>SEMESTRAL</h4>
                  <p className={styles.price}>Por {config.memberships.find(m => m.name === 'vip6').price}$ cada 6 meses</p>
                </div>
              </div>
            </div>
            <div className={styles.foot}>
              <Button small onClick={() => setVipModal(!vipModal)}>Cerrar</Button>
            </div>
          </Card>
        </Overlay>
      )
    }
  }

  return (
    <React.Fragment>
      <table className={styles.table}>
        <thead className={styles.head}>
          <tr className={styles.headRow}>
            <th className={styles.titleColumn} colSpan="2">
              <img className={styles.headLogo} src={b4sTypo} alt="subLogo" />
            </th>
            <th className={styles.rowContent}>
              <div>
                <h3 className={styles.standard}>STANDARD</h3>
                <p className={styles.headingSub}>{config.memberships.find(m => m.name === 'standard').price}$/mes</p>
              </div>
            </th>
            <th className={styles.rowContent}>
              <div>
                <h3 className={styles.vip}>VIP</h3>
                <p className={styles.headingSub}>{config.memberships.find(m => m.name === 'vip').price}$/mes o {config.memberships.find(m => m.name === 'vip6').price}$/6 meses</p>
              </div>
            </th>
          </tr>
        </thead>
        <tbody className={styles.body}>
          <tr className={styles.bodyRow}>
            <td className={styles.titleColumn} colSpan="2">
              <div>
                <p className={styles.title}>Descarga beats</p>
                <p className={styles.description}>Descarga inmediatamente en formato wav sin marca de agua</p>
              </div>
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
          </tr>
          <tr className={styles.bodyRow}>
            <td className={styles.titleColumn} colSpan="2">
              <div>
                <p className={styles.title}>Monetiza tus canciones en Youtube</p>
                <p className={styles.description}>Gana dinero con tu música</p>
              </div>
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
          </tr>
          <tr className={styles.bodyRow}>
            <td className={styles.titleColumn} colSpan="2">
              <div>
                <p className={styles.title}>Sube tus canciones a las plataformas</p>
                <p className={styles.description}>Spotify, iTunes, Deezer, etc. Sin ninguna limitación</p>
              </div>
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
          </tr>
          <tr className={styles.bodyRow}>
            <td className={styles.titleColumn} colSpan="2">
              <div>
                <p className={styles.title}>Cancela cuando quieras</p>
              </div>
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
          </tr>
          <tr className={styles.bodyRow}>
            <td className={styles.titleColumn} colSpan="2">
              <div>
                <p className={styles.title}>Descarga las pistas de cada beat</p>
                <p className={styles.description}>Descarga el trackout, la forma más profesional a tu alcance</p>
              </div>
            </td>
            <td className={styles.rowContent}>
              <img className={styles.lineImg} src={line} alt="no" />
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
          </tr>
          <tr className={styles.bodyRow}>
            <td className={styles.titleColumn} colSpan="2">
              <div>
                <p className={styles.title}>Acceso a mezclas PRO</p>
                <p className={styles.description}>Envía tus temas por pistas y nuestros ingenieros los harán sonar con la mejor calidad que te puedes imaginar, tenemos un precio especial fijo de solo 40$ por este servicio</p>
              </div>
            </td>
            <td className={styles.rowContent}>
              <img className={styles.lineImg} src={line} alt="no" />
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
          </tr>
          <tr className={styles.bodyRow}>
            <td className={styles.titleColumn} colSpan="2">
              <div>
                <p className={styles.title}>Acceso a oportunidades y premios</p>
                <p className={styles.description}>Creamos concursos donde puedes ganar premios en metálico y colaboraciones con otros artistas</p>
              </div>
            </td>
            <td className={styles.rowContent}>
              <img className={styles.lineImg} src={line} alt="no" />
            </td>
            <td className={styles.rowContent}>
              <img className={styles.checkImg} src={check} alt="yes" />
            </td>
          </tr>
        </tbody>
        <tfoot className={styles.foot}>
          <tr className={styles.footRow}>
            <td className={styles.footSpace} colSpan="2"></td>
            <td className={styles.rowContent} >
              <Button dark onClick={() => props.buttonAction('standard')}>Únete a Standard</Button>
            </td>
            <td className={styles.rowContent}>
              <Button onClick={() => setVipModal(!vipModal)}>Únete a VIP</Button>
            </td>
          </tr>
        </tfoot>
      </table>
      {renderModal()}
    </React.Fragment>
  )
}

export default Subscriptiontable
