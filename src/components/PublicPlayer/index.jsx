import React, {useState, useEffect, useRef} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import MusicPlayer from '../MusicPlayer'
import {getBeats} from '../../actions/public_actions'
import beatsSelector from '../../selectors/beatsSelector'

const PublicPlayer = () => {
  const dispatch = useDispatch()
  const [filter, setFilter] = useState('') // Possible values: NEW, CAT1, CAT2, CAT3...
  const {beats, isLoading, error} = useSelector(state => (
    {
      beats: beatsSelector(state.beats.beats, filter),
      isLoading: state.beats.isLoading,
      error: state.beats.error
    }
  ))

  
  //Side-Effect que descarga los beats de la API en Redux si no hay
  const hasBeats = useRef(false)
  useEffect(() => {
    if (hasBeats.current === false) {
      dispatch(getBeats())
      hasBeats.current = true
    }
  }, [hasBeats, dispatch])


  const disableBeatFilter = () => {
    setFilter()
  }

  const filterNewBeats = () => {
    setFilter('NEW')
  }


  const filterCategoryBeats = category => {
    setFilter(`CAT${category}`)
  }

  return (
    <MusicPlayer 
      playerType='public'
      beats={beats}
      isLoading={isLoading}
      error={error}
      disableBeatFilter={disableBeatFilter}
      filterNewBeats={filterNewBeats}
      filterCategoryBeats={filterCategoryBeats}
    />
  )

}
export default PublicPlayer
