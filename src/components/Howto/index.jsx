import React, {Component} from 'react'
import styles from './Howto.module.scss'

import NewspaperIllu from '../../assets/img/howto-newspaper.svg'
import DownloadsIllu from '../../assets/img/howto-downloads.svg'
import UserAreaIllu from '../../assets/img/howto-user-area.svg'
import SubscriptionsIllu from '../../assets/img/howto-subscriptions.svg'
import DontIllu from '../../assets/img/howto-dont.svg'
import EmailIllu from '../../assets/img/howto-email.svg'


class Howto extends Component {


  render() {

    return (
      <React.Fragment>
        <div className={styles.features}>
          <div className={styles.container}>
            <div className={styles.featuresBody}>
              <div className={`${styles.feature} ${styles.productores}`}>
                <div className={styles.featureText}>
                  <h3 className={styles.featureTitle}>Novedades de la versión 2.0 de Beats4seven</h3>
                  <ul className={styles.featureDesc}>
                    <li>Se ha rediseñado completamente la web, para mejorar la velocidad y la experiencia de usuario.</li>
                    <li>Nuevo reproductor. Más rápido, con más categorías de beats y posibilidad de añadir beats como favoritos.</li>
                    <li>Las descargas de beats funcionan a través de un sistema de créditos. Con muchos beneficios para los usuarios como la posibilidad de acumular descargas de un mes a otro.</li>
                    <li>Se ha añadido el acceso a un servicio de mezcla para los usuarios VIP, ahora pueden enviar las pistas de la canción, incluídas las voces, para ser mezcladas y masterizadas… consiguiendo una calidad profesional en el resultado final. Este servicio tiene un coste reducido, normalmente cuesta 120/150 $, nosotros contamos con profesionales que lo harán solo para Beats4seven por 40 $.</li>
                    <li>Los usuarios Vip tendrán acceso a concursos y sorteos especiales que se harán de forma interna en la web. Con premios en metálico, créditos y colaboraciones.</li>
                  </ul>
                </div>
                <div className={styles.marginLeft}>
                  <img className={styles.producerImg} src={NewspaperIllu} alt="Periódico" />
                </div>
              </div>

              <div className={`${styles.feature} ${styles.beats}`}>
                <div className={styles.marginRight}>
                  <img className={styles.beatsImg} src={DownloadsIllu} alt="Beats en Beats4seven" />
                </div>
                <div className={styles.featureText}>
                  <h3 className={styles.featureTitle}>Sistema de descargas</h3>
                  <ul className={styles.featureDesc}>
                    <li>Las descargas se realizan a través de créditos, que recibes cuando te registras o haces el pago de una mensualidad. Recibiendo 4 créditos al inicio y 4 cada vez que realizas un pago.</li>
                    <li>Cada beat te cuesta 1 crédito..</li>
                    <li>Si te sobran créditos un mes, se suman a los créditos que recibes al mes siguiente. Por lo que puedes descargar por ejemplo 6 beats en un mes.</li>
                    <li>Puedes usar los créditos siempre y cuando estés con tu cuenta activa, si te das de baja pierdes los créditos acumulados, pero los beats que descargaste los puedes seguir usando siempre sin problemas, monetizar con ellos y subirlos a plataformas.</li>
                  </ul>
                </div>
              </div>

              <div className={`${styles.feature} ${styles.productores}`}>
                <div className={styles.featureText}>
                  <h3 className={styles.featureTitle}>Panel de usuario</h3>
                  <p>Beats</p>
                  <ul className={styles.featureDesc}>
                    <li>Haz clic en el diamante del reproductor para descargar un beat usando 1 crédito. </li>
                    <li>Todas tus descargas las tienes en la pestaña tus beats del reproductor.</li>
                  </ul>
                  <p>Suscripción</p>
                  <ul className={styles.featureDesc}>
                    <li>Desde la pestaña suscripción puedes ver tu plan elegido, método de pago y cancelar tu cuenta.</li>
                  </ul>
                </div>
                <div className={styles.marginLeft}>
                  <img className={styles.producerImg} src={UserAreaIllu} alt="Panel de usuario" />
                </div>
              </div>

              <div className={`${styles.feature} ${styles.productores}`}>
                <div className={styles.marginRight}>
                  <img className={styles.beatsImg} src={SubscriptionsIllu} alt="Suscripciones" />
                </div>
                <div className={styles.featureText}>
                  <h3 className={styles.featureTitle}>Tu cuenta</h3>
                  <ul className={styles.featureDesc}>
                    <li>Te puedes registrar en el plan standard ( 7 $ ), Vip ( 12 $ ) o Vip 6 meses ( 9 $ al mes ).</li>
                    <li>Una vez realizas el pago se te vuelve a cobrar cada 30 días, excepto en el plan Vip 6 meses, en el cual se te cobrarán 54$ cada 6 meses.</li>
                    <li>Puedes cancelar cuando quieras con dos clics y sin coste alguno.</li>
                  </ul>
                </div>

              </div>

              <div className={`${styles.feature} ${styles.beats}`}>

                <div className={styles.featureText}>
                  <h3 className={styles.featureTitle}>Qué puedes hacer con nuestros beats y qué no</h3>
                  <ul className={styles.featureDesc}>
                    <li>Para usar nuestros beats es necesario tener una cuenta y haber descargado el beat, tenerlo en tu histórico de descargas. No importa que canceles tu cuenta, si una vez lo descargaste será tuyo y lo podrás seguir usando siempre.</li>
                    <li>En cada publicación que hagas de la canción final, debes nombrar al productor, siempre. De la siguiente forma en la descripción… “ música por ( nombre del productor )”.</li>
                    <li>Queda totalmente prohibido el registro del content id de la canción, ya que puede generar problemas a otros usuarios y a los productores.</li>
                    <li>Nuestras licencias de beats no tienen límite de uso respecto a streams en plataformas o visualizaciones en Youtube y puedes monetizar tus canciones con ellas en cualquier plataforma.</li>
                  </ul>
                </div>
                <div className={styles.marginLeft}>
                  <img className={styles.producerImg} src={DontIllu} alt="Do/Don't" />
                </div>
              </div>

              <div className={`${styles.feature} ${styles.productores}`}>
                <div className={styles.marginRight}>
                  <img className={styles.beatsImg} src={EmailIllu} alt="Email" />
                </div>
                <div className={styles.featureText}>
                  <h3 className={styles.featureTitle}>Preguntas y respuestas frecuentes</h3>
                  <p className={styles.featureDesc}>
              Si tienes cualquier duda, en el pie de páginas tienes un enlace a la página de preguntas frecuentes ( FAQ ). Si no encuentras respuesta nos puedes escribir a info@beats4seven.com
                  </p>
                </div>
              </div>

            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}


export default Howto