import React, {useState} from 'react'
import axios from 'axios'
import ReactPixel from 'react-facebook-pixel'

import Button from '../Button'
import Alert from '../Alert'
import Card from '../Card'
import {Input, Textarea} from '../FormElements'
import {formularioContacto} from '../../schemas/formularioContacto'
import styles from './Contacto.module.scss'
import config from '../../config'

const Contacto = props => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')
  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)
  const [error, setError] = useState(false)
  const [errors, setErrors] = useState(false)

  const onSubmit = async () => {
    setError(false)
    setLoading(true)

    try {
      //Validacion de campos
      await formularioContacto.validate({name,email,message}, {abortEarly: false})


      const formData = new FormData()
      formData.set('your-name', name)
      formData.set('your-email', email)
      formData.set('your-message', message)

      await axios({
        method: 'post', 
        url: `${config.domain}/contact-form-7/v1/contact-forms/${props.formid}/feedback`, 
        data: formData,
        headers: {'content-type': 'multipart/form-data'}
      })
      setSuccess(true)
      setLoading(false)
      ReactPixel.track('Contact')
    } catch (err) {
      setError(true)
      setLoading(false)
      if (err.name === 'ValidationError') {
        // Mapeo de una lista de errores a
        // Un objeto de un error por key (nombre campo)
        let errors = {}
        err.inner.forEach(one => {
          errors = {...errors, [one.path]: one.message}
        })
        setErrors(errors)
      }
    }
  }

  const showError = () => {
    if (error) {
      return <Alert>Hubo un error enviando tu solicitud</Alert>
    }
  }

  const showSuccess = () => {
    if (success) {
      return <Alert success>Gracias por ponerte en contacto con el soporte, te responderemos lo antes posible.</Alert>
    }
  }



  return (
    <React.Fragment>
      {showError()}
      {showSuccess()}
      <Card className={styles.card} hasPadding>
        <Input placeholder="Nombre" onChange={(e) => setName(e.target.value)} error={errors.name} />
        <Input placeholder="E-Mail" onChange={(e) => setEmail(e.target.value)} error={errors.email} />
        <Textarea placeholder="Tu mensaje" onChange={(e) => setMessage(e.target.value)} error={errors.message} />
        <Button isLoading={loading} onClick={() => onSubmit()}>Enviar</Button>
      </Card>
    </React.Fragment>
  )
}


export default Contacto