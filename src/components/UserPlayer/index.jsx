import React, {useState, useEffect} from 'react'
import {useDispatch} from 'react-redux'
import {setFavouriteUserBeat, purchaseUserBeat, upgradeUserBeat, downloadUserBeat} from '../../actions/user_actions'
import MusicPlayer from '../MusicPlayer'
import beatsSelector from '../../selectors/beatsSelector'
import Icon from '../Icon'
import Overlay from '../Overlay'
import Card from '../Card/index.jsx'
import Button from '../Button/index.jsx'
import styles from './UserPlayer.module.scss'


const UserPlayer = props => {
  const dispatch = useDispatch()
  const [beats, setBeats] = useState([])
  const [purchaseModal, enablePurchaseModal] = useState(null)
  const [upgradeModal, enableUpgradeModal] = useState(null)
  const [filter, setFilter] = useState('') // Possible values: NEW, USER, CAT1, CAT2, CAT3...

  useEffect(() => {
    setBeats(beatsSelector(props.beats, filter))
  }, [dispatch, beats, props.beats, filter])

  const disableBeatFilter = () => {
    setFilter()
  }

  const filterUserBeats = () => {
    setFilter('USER')
  }

  const filterUserFavouriteBeats = () => {
    setFilter('FAVOURITE')
  }

  const filterNewBeats = () => {
    setFilter('NEW')
  }


  const filterCategoryBeats = category => {
    setFilter(`CAT${category}`)
  }

  const renderFavouriteAction = b => {
    const selected = b.favourite ? true : false
    return (
      <Icon 
        onClick={() => dispatch(setFavouriteUserBeat(b.id))}
        dataTip={selected ? "Quitar beat de tus favoritos" : "Añadir beat a tus favoritos"}
        primaryicon="far fa-heart"
        primarystyle={`${styles.actionIcon}`}
        selected={selected}
        selectedicon="fas fa-heart"
        selectedstyle={`${styles.actionIcon} ${styles.selected}`}
      />
    )
  }

  const renderUpgradeAction = b => {
    if (b.purchased && b.subscription === 'standard' && props.role === 'vip') {

      return (
        <Icon 
          onClick={() => enableUpgradeModal({id: b.id, name: b.name})}
          dataTip="Actualizar beat a VIP por 1 crédito para incluir los trackouts"
          primaryicon="fas fa-angle-double-up"
          primarystyle={`${styles.actionIcon}`}
          loadingstyle={`${styles.actionIcon} ${styles.waitIcon}`}
          loading={b.isUpLoading}
        />
      )
    }
  }

  const renderPurchaseAction = b => {
    if (!b.purchased) {
      const loading = b.isLoading
      return (
        <Icon 
          onClick={() => enablePurchaseModal({id: b.id, name: b.name})}
          dataTip="Comprar por 1 crédito"
          primaryicon="far fa-gem"
          primarystyle={`${styles.actionIcon}`}
          loadingicon="fas fa-sync-alt fa-spin"
          loadingstyle={`${styles.actionIcon} ${styles.waitIcon}`}
          loading={loading}
        />
      )
    }
  }

  const renderDownloadAction = b => {
    if (b.purchased) {
      const loading = b.isLoading
      return (
        <Icon 
          onClick={() => !loading && dispatch(downloadUserBeat(b.id))}
          dataTip={loading ? "Descargando beat..." : "Descargar beat"}
          primaryicon="fas fa-download"
          primarystyle={`${styles.actionIcon}`}
          loadingicon="fas fa-sync-alt fa-spin"
          loadingstyle={`${styles.actionIcon} ${styles.waitIcon}`}
          loading={loading}
        />
      )
    }
  }


  const renderPurchaseModal = () => {
    if (purchaseModal === null) return
    return (
      <Overlay
        onClick={() => enablePurchaseModal(null)}
      >
        {props.credits === 0 && NoCreditsCard()}
        {props.credits > 0 && PurchaseCard()}
      </Overlay>
    )
  }

  const PurchaseCard = () => {
    const {id, name} = purchaseModal
    return (
      <Card className={styles.modal}>
        <h4>Estas a punto de gastar 1 crédito</h4>
        <p>Por favor, confirma que quieres <b>{name}</b> por 1 crédito</p>
        <div className={styles.buttonBar}>
          <Button 
            primary 
            onClick={() => purchaseModalAction(id)}
          >Si</Button>
          <Button 
            dark
            onClick={() => enablePurchaseModal(null)}
          >No</Button>
        </div>
      </Card>
    )
  }

  const NoCreditsCard = () => {
    return (
      <Card className={styles.modal}>
        <h4>No quedan créditos disponibles</h4>
        <p>Cuando tu suscripción se renueve podrás canjear mas beats</p>
        <div className={styles.buttonBar}>
          <Button 
            dark
            onClick={() => enablePurchaseModal(null)}
          >Cerrar</Button>
        </div>
      </Card>
    )
  }

  const purchaseModalAction = id => {
    enablePurchaseModal(null)
    dispatch(purchaseUserBeat(id))
  }

  const renderUpgradeModal = () => {
    if (upgradeModal === null) return
    const {id, name} = upgradeModal
    return (
      <Overlay
        onClick={() => enableUpgradeModal(null)}
      >
        <Card className={styles.modal}>
          <h4>Estas a punto de gastar 1 crédito</h4>
          <p>Por favor, confirma que quieres desbloquear los trackouts de <b>{name}</b> por 1 crédito</p>
          <div className={styles.buttonBar}>
            <Button 
              primary 
              onClick={() => upgradeModalAction(id)}
            >Si</Button>
            <Button 
              dark
              onClick={() => enableUpgradeModal(null)}
            >No</Button>
          </div>
        </Card>
      </Overlay>
    )
  }

  const upgradeModalAction = id => {
    enableUpgradeModal(null)
    dispatch(upgradeUserBeat(id))
  }

  return (
    <MusicPlayer 
      playerType='user'
      beats={beats}
      oneRowPlayer={props.oneRowPlayer ? true : false}
      role={props.role}
      isLoading={props.isLoading}
      error={props.error}
      disableBeatFilter={disableBeatFilter}
      filterNewBeats={filterNewBeats}
      filterUserBeats={filterUserBeats}
      filterUserFavouriteBeats={filterUserFavouriteBeats}
      filterCategoryBeats={filterCategoryBeats}
      renderDownloadAction={renderDownloadAction}
      renderFavouriteAction={renderFavouriteAction}
      renderPurchaseAction={renderPurchaseAction}
      renderUpgradeAction={renderUpgradeAction}
      renderUpgradeModal={renderUpgradeModal}
      renderPurchaseModal={renderPurchaseModal}
    />
  )
}

export default UserPlayer
