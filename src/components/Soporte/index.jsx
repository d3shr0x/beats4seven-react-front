import React, {useState} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import Button from '../Button'
import Alert from '../Alert'
import Card from '../Card'
import {Textarea} from '../FormElements'
import styles from './Soporte.module.scss'
import config from '../../config'

const Soporte = props => {
  const [message, setMessage] = useState('')
  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)
  const [error, setError] = useState(false)
  const [errors, setErrors] = useState(false)


  const onSubmit = async () => {
    setError(false)
    setLoading(true)
    try {

      const formData = new FormData()
      formData.set('your-userid', props.userid)
      formData.set('your-username', props.username)
      formData.set('your-name', props.name)
      formData.set('your-email', props.email)
      formData.set('your-message', message)

      await axios({
        method: 'post', 
        url: `${config.domain}/contact-form-7/v1/contact-forms/${props.formid}/feedback`, 
        data: formData,
        headers: {'content-type': 'multipart/form-data'}
      })
      setSuccess(true)
      setLoading(false)

    } catch (err) {
      setError(true)
      setLoading(false)
      if (err.name === 'ValidationError') {
        // Mapeo de una lista de errores a
        // Un objeto de un error por key (nombre campo)
        let errors = {}
        err.inner.forEach(one => {
          errors = {...errors, [one.path]: one.message}
        })
        setErrors(errors)
      }
    }
  }

  const showError = () => {
    if (error) {
      return <Alert>Hubo un error enviando tu solicitud</Alert>
    }
  }

  const showSuccess = () => {
    if (success) {
      return <Alert success>Gracias por ponerte en contacto con el soporte, te responderemos lo antes posible.</Alert>
    }
  }



  return (
    <React.Fragment>
      {showError()}
      {showSuccess()}
      <Card className={styles.card} hasPadding>
        <h4 className={styles.h4}>Soporte</h4>
        <p className={styles.p}>Si tienes dudas, problemas o simplemente quieres compartir tus halagos (que nos encantaria leer) deja tu mensaje por aquí, te responderemos en 24 horas. <br/>
        Comprueba nuestras <Link to="/faq">FAQ</Link> antes de mandarnos tu duda.</p>
        <Textarea placeholder="Tu mensaje" onChange={(e) => setMessage(e.target.value)} error={errors.message} />
        <Button isLoading={loading} onClick={() => onSubmit()}>Enviar</Button>
      </Card>
    </React.Fragment>
  )
}


export default Soporte