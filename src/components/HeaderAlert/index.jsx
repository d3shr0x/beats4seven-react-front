import React from 'react'
import styles from './HeaderAlert.module.scss'

const HeaderAlert = props => {
  
  let classes = `${styles.headerAlert} ${props.className}`
  if (props.hasPadding) classes = `${classes} ${styles.hasPadding}`

  return (
    <div 
      className={classes}
      id={props.id}
    >
      <h2>{props.title}</h2>
      <p>{props.message}</p>
    </div>
  )

}


export default HeaderAlert
