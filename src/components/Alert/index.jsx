import React from 'react'
import posed from 'react-pose'

import styles from './Alert.module.scss'

const StaticAlert = React.forwardRef((props, ref) => {
  let className = `${styles.alert} ${props.className}`
  if (props.success) className = `${className} ${styles.success}`
  
  return (
    <div ref={ref} className={className}>
      {props.children}
    </div>
  )
})

const Alert = posed(StaticAlert)({
  enter: {
    y: 0,
    opacity: 1,
    transition: {
      y: {type: 'spring', stiffness: 100},
      default: {duration: 300}
    }
  },
  exit: {
    y: -60,
    opacity: 0,
    transition: {duration: 300}
  }
})

export default Alert