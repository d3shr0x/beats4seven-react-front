import React, {Component} from 'react'
import styles from './Button.module.scss'


class Button extends Component {

  render() {
    let classes = `${styles.button}`
    let disabled = false
    let onClick = undefined
    let loadingIcon = ''
    if (this.props.primary || (!this.props.primary && !this.props.dark && !this.props.light && !this.props.transparent)) {
      classes = `${classes} ${styles.primary}`
    } 

    if (this.props.dark) classes = `${classes} ${styles.dark}`
    if (this.props.light) classes = `${classes} ${styles.light}`
    if (this.props.transparent) classes = `${classes} ${styles.transparent}`
    if (this.props.small) classes = `${classes} ${styles.small}`

    if (this.props.className) classes = `${classes} ${this.props.className}`

    if (this.props.isLoading) {
      disabled = 'disabled'
      classes =  `${classes} ${styles.disabled}`
      loadingIcon = <i className="fal fa-sync fa-spin" />
    }

    if (this.props.onClick) {
      onClick = this.props.onClick
    }
    
    return (
      <button className={classes} onClick={onClick} disabled={disabled} >{this.props.children} {loadingIcon}</button>
    )
  }
}


export default Button