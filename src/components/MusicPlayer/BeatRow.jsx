import React, {useState, useEffect, useRef, useCallback, Fragment} from 'react'
import {Link as HashLink} from "react-scroll"
import config from '../../config'
import Icon from '../Icon'
import styles from './MusicPlayer.module.scss'


const BeatRow = props => {
  const [seek,updateSeek] = useState(0)
  const [duration,updateDuration] = useState(0)
    
  const {beat, isActiveBeat, isPlaying, howlerLoaded} = props
  let timer = useRef(null)
  const minRange = 0
  const loop = 600
  const src = config.s3mp3dir+beat.filename+'.mp3'


  //Side-Effect que cancela el timer
  //Relacionado con la barra de duracion
  // useEffect(() => clearInterval(timer.current))


  //Funcion que actualiza el state con el segundo actual de la cancion
  const renderSeekPos = useCallback(() => {
    if (window.Howler && window.Howler._howls.length > 0) {
      updateSeek(window.Howler._howls[0].seek())
      updateDuration(window.Howler._howls[0].duration())
    }
  }, [])

  //Side-Effect que arranca howler o para el intervalo segun necesario
  useEffect(()=>{
    if (howlerLoaded && isPlaying) {
      timer.current = setInterval(renderSeekPos, loop)
    } else {
      clearInterval(timer.current)
    }
  },[howlerLoaded, isActiveBeat, isPlaying, renderSeekPos])
    

  return (
    <tr>
      <td className={styles.play}>
        <Icon 
          onClick={() => props.playSong(src, beat.id)}
          primaryicon="fas fa-play"
          primarystyle={`${styles.actionIcon} ${styles.selected}`}
          selected={isPlaying}
          selectedicon="fas fa-pause"
          selectedstyle={`${styles.actionIcon} ${styles.selected}`}
        />
      </td>
      <td className={styles.name}>{beat.name}</td>
      <td className={styles.length}>
        <div className={styles.flexBox}>
          <input 
            type="range" 
            min={minRange} 
            max={duration} 
            value={isActiveBeat ? seek : 0} 
            onChange={e => isActiveBeat && props.updateActiveBeatSeek(e.target.value)}
          />
        </div>
      </td>
      <td className={styles.actions}>
        <div className={styles.flexBox}>
          {props.playerType === 'public' && 
            <HashLink smooth={true} to="planes">
              <Icon 
                dataTip="Comprar por 1 crédito"
                primaryicon="far fa-gem"
                primarystyle={`${styles.actionIcon}`}
                selectedicon="fas fa-download"
                selectedstyle={`${styles.actionIcon} ${styles.selected}`}
              />
            </HashLink>
          }
          {props.playerType === 'user' && (
            <Fragment>
              {props.renderFavouriteAction(beat)}
              {props.renderUpgradeAction(beat)}
              {props.renderPurchaseAction(beat)}
              {props.renderDownloadAction(beat)}
            </Fragment>
          )}
        </div>
      </td>
    </tr>
  )
}

export default BeatRow
