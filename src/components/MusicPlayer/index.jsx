import React, {useState, Fragment} from 'react'
import ReactHowler from '@qazsero/react-howler'
import config from '../../config'
import BeatRow from './BeatRow'
import styles from './MusicPlayer.module.scss'
import './InputRange.module.scss'

const MusicPlayer = props => {
  const [activeBeat,updateActiveBeat] = useState({src: '', id:0, playing: false, active: false})
  const [howlerLoaded, loadHowler] = useState(false)

  //classnames
  let classes = `${styles.player}`
  if (props.oneRowPlayer) classes = `${classes} ${styles.oneRow}`


  const playSong = (src, id) => {
    //Si reproduzco una cancion
    //*Y vuelvo a dar play a la misma, quiero que haga la acción contraria (reproducir o parar)
    //*Y le doy al play a otra cancion, esa cancion se reproduce
    updateActiveBeat(oldSong => ({src, id, playing: id === oldSong.id ? !oldSong.playing : true, active: true}))
  }

  if (window.Howler) {
    window.Howler.autoUnlock = false
  }

  const updateActiveBeatSeek = e => {
    if (window.Howler && window.Howler._howls.length > 0) {
      window.Howler._howls[0].seek(e)
    }
  }

  const audioPlayer = () => {
    if (activeBeat.active === true) {
      return (
        <ReactHowler
          src={activeBeat.src}
          playing={activeBeat.playing}
          onLoad={() => loadHowler(true)}
          onEnd={() => playNextSongOrStop()}
          html5={true}
        />
      )
    }
  }
  
  const playNextSongOrStop = () => {
    //Sacamos el ultimo elemento de la lista de canciones
    //Lo comparamos con la cancion que ha parado de reproducirse
    const lastBeat = props.beats.slice(-1)
    const index = props.beats.findIndex(a => a.id === activeBeat.id)
    if (activeBeat.id === lastBeat.id) {
      updateActiveBeat({src:'',id:0, playing: false})
      loadHowler(false)
    } else if (index === 0) {
      const src = config.s3mp3dir+props.beats[0].filename+'.mp3'
      playSong(src,props.beats[0].id)
    } else {
      const src = config.s3mp3dir+props.beats[index+1].filename+'.mp3'
      playSong(src, props.beats[index+1].id)
    }
  }



  const renderTable = () => {
    return (
      <table>
        <thead>
          <tr>
            <th className={styles.play}>Play</th>
            <th className={styles.name}>Nombre</th>
            <th className={styles.length}>Duración</th>
            <th className={styles.actions}>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {props.beats.map((beat, i) => 
            <BeatRow
              beat={beat}
              key={i}
              playerType={props.playerType}
              isActiveBeat={activeBeat.id === beat.id}
              isPlaying = {activeBeat && activeBeat.id === beat.id && activeBeat.playing === true}
              howlerLoaded={howlerLoaded}
              playSong={playSong}
              updateActiveBeatSeek={updateActiveBeatSeek}
              renderFavouriteAction={props.renderFavouriteAction}
              renderUpgradeAction={props.renderUpgradeAction}
              renderPurchaseAction={props.renderPurchaseAction}
              renderDownloadAction={props.renderDownloadAction}
            />
          )}
        </tbody>
      </table>
    )
  }

  const renderLoading = () => (
    <div className={styles.loading}>
      <i className="fal fa-spin fa-compact-disc"></i>
    </div>
  )
  
  const renderError = () => (
    <div>
      Error al cargar. Reintentar
    </div>
  )


  return (
    <div className={`${classes}`}>
      {!props.oneRowPlayer &&
        <div className={styles.sidebar}>
          <div className={styles.sidebarContent}>
            <ul className={styles.quickList}>
              <li><button onClick={() => props.disableBeatFilter()}>Todo</button></li>
              {props.playerType === 'user' && (
                <Fragment>
                  <li><button className={styles.navLink} onClick={() => props.filterUserBeats()}>Tus beats</button></li>
                  <li className={`${styles.iconItem} ${styles.active}`}><button className={styles.navLink} onClick={() => props.filterUserFavouriteBeats()}>Favoritos</button><i className={`fas fa-heart ${styles.icon}`}></i></li>
                </Fragment>
              )}
              <li className={styles.iconItem}><button onClick={() => props.filterNewBeats()}>Novedades</button><i className={`fas fa-star ${styles.icon}`}></i></li>
            </ul>

            <p className={styles.title}>CATEGORIAS</p>
            <ul className={styles.catList}>
              <li><button className={styles.navLink} onClick={() => props.filterCategoryBeats(1)}>Latin Music</button></li>
              <li><button className={styles.navLink} onClick={() => props.filterCategoryBeats(2)}>Afrobeats</button></li>
              <li><button className={styles.navLink} onClick={() => props.filterCategoryBeats(3)}>Hip Hop / Rap</button></li>
              <li><button className={styles.navLink} onClick={() => props.filterCategoryBeats(4)}>R&B / Trapsoul</button></li>
              <li><button className={styles.navLink} onClick={() => props.filterCategoryBeats(5)}>Pop / Club</button></li>
              <li><button className={styles.navLink} onClick={() => props.filterCategoryBeats(6)}>Trap</button></li>
            </ul>
          </div>

        </div>
      }
      <div className={styles.songList}>
        {props.isLoading && renderLoading()}
        {props.error && renderError()}
        {props.beats && props.beats.length > 0 && renderTable()}
      </div>
      {audioPlayer()}
      {props.playerType === 'user' && props.renderPurchaseModal()}
      {props.playerType === 'user' && props.renderUpgradeModal()}
    </div>
  )
}

export default MusicPlayer
