import React, {Component} from 'react'
import {motion} from "framer-motion"
import styles from './Icon.module.scss'
import 'rc-tooltip/assets/bootstrap.css'

class Icon extends Component {

  render() {

    let classes = `${styles.icon}`

    if (this.props.loading === true) {
      if (this.props.loadingicon) classes = `${classes} ${this.props.loadingicon}`
      if (this.props.loadingstyle) classes = `${classes} ${this.props.loadingstyle}`
    } else {
      if (!this.props.selected) {
        if (this.props.primaryicon) classes = `${classes} ${this.props.primaryicon}`
        if (this.props.primarystyle) classes = `${classes} ${this.props.primarystyle}`
      }
  
      if (this.props.selected === true) {
        if (this.props.selectedicon) classes = `${classes} ${this.props.selectedicon}`
        if (this.props.selectedstyle) classes = `${classes} ${this.props.selectedstyle}`
      }
    }

    return (
      <div className={styles.container}>
        <motion.i
          id={this.props.id}
          onClick={this.props.onClick}
          data-tip={this.props.dataTip}
          className={classes} 
          whileHover={{scale: 1.2}}
        />
        {this.props.dataTip && <div className={styles.tooltip}>{this.props.dataTip}</div>}
      </div>
    )
  }
}


export default Icon
