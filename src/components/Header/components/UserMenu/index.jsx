import React from 'react'
import posed from 'react-pose'
import {Link} from 'react-router-dom'
import Card from '../../../../components/Card'

import styles from './UserMenu.module.scss'

const UserMenuRef = React.forwardRef((props, ref) => (
  <div ref={ref}>
    <Card className={styles.userMenu}>
      <ul>
        <li><Link to="/user-area">Área de usuario</Link></li>
        <li onClick={() => props.userSignout()}>Desconectar</li>
      </ul>
    </Card>
  </div>

))

const UserMenu = posed(UserMenuRef)({
  enter: {
    y: 0,
    opacity: 1,
    transition: {
      y: {type: 'spring', stiffness: 100},
      default: {duration: 300}
    }
  },
  exit: {
    y: -30,
    opacity: 0,
    transition: {duration: 300}
  }
})

export default UserMenu