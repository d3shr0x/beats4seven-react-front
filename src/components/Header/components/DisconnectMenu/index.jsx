import React from 'react'
import posed from 'react-pose'
import Card from '../../../../components/Card'

import styles from './DisconnectMenu.module.scss'

const DisconnectMenuRef = React.forwardRef((props, ref) => (
  <div ref={ref}>
    <Card className={styles.userMenu}>
      <ul>
        <li onClick={() => props.userSignout()}>Desconectar</li>
      </ul>
    </Card>
  </div>

))

const DisconnectMenu = posed(DisconnectMenuRef)({
  enter: {
    y: 0,
    opacity: 1,
    transition: {
      y: {type: 'spring', stiffness: 100},
      default: {duration: 300}
    }
  },
  exit: {
    y: -30,
    opacity: 0,
    transition: {duration: 300}
  }
})

export default DisconnectMenu