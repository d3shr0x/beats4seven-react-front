import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {PoseGroup} from 'react-pose'
import {userSignout} from '../../actions/auth_actions'
import UserMenu from './components/UserMenu'
import DisconnectMenu from './components/DisconnectMenu'
import Button from '../../components/Button'
import styles from './Header.module.scss'



class Header extends Component {
  state={
    userMenu: false,
    gemMenu: false
  }

  publicArea = () => (
    <div className={styles.login}>
      <Link to="/login"><Button light small>Login</Button></Link>
    </div>
  )

  privateArea = () => (
    <div className={styles.userStatus}>
      <div className={styles.userCredits}>
        <p>Hola <span className={styles.bold}>{this.props.displayname}</span>, tienes</p>
        <p className={styles.credits}>{this.props.credits}</p>
        <i className={`fas fa-gem ${styles.gemIcon}`}></i>
      </div>

      <div className={styles.userOptions}>
        <i onClick={() => this.setState(({userMenu}) => ({userMenu: !userMenu}))} className={`fas fa-user-circle ${styles.icon}`}></i>
        <PoseGroup>
          {this.showUserMenu()}
        </PoseGroup>
      </div>
    </div>
  )

  disconnectArea = () => (
    <div className={styles.userStatus}>
      <div className={styles.userCredits}>
        <p>Hola <span className={styles.bold}>{this.props.displayname}</span></p>
      </div>

      <div className={styles.userOptions}>
        <i onClick={() => this.setState(({userMenu}) => ({userMenu: !userMenu}))} className={`fas fa-user-circle ${styles.icon}`}></i>
        <PoseGroup>
          {this.showDisconnectMenu()}
        </PoseGroup>
      </div>
    </div>
  )

  showUserMenu = () => {
    if (this.state.userMenu) {
      return (
        <UserMenu key="userMenu" userSignout={() => this.props.userSignout()} />
      )
    }

  }

  showDisconnectMenu = () => {
    if (this.state.userMenu) {
      return (
        <DisconnectMenu key="userMenu" userSignout={() => this.props.userSignout()} />
      )
    }

  }

  render() {
    const b4sTypo = require('../../assets/img/logo-typo.svg')

    return (
      <div className={styles.header}>
        <div className={styles.logo}>
          <Link to="/"><img alt="logo" src={b4sTypo} /></Link>
        </div>
        {['vip', 'standard', 'expired'].includes(this.props.role) && this.privateArea()}
        {['affiliate', 'onboarding'].includes(this.props.role) && this.disconnectArea()}
        {this.props.role === 'guest' && this.publicArea()}
      </div>
    )
  }
}

const mapStateToProps = ({user}) => ({
  credits: user.credits,
  displayname: user.displayname,
  token: user.token,
  role: user.role
})


export default connect(mapStateToProps, {userSignout})(Header)
