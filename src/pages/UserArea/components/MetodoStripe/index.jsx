import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Link, Redirect} from 'react-router-dom'
import {
  CardElement,
  StripeProvider,
  Elements,
  injectStripe,
} from 'react-stripe-elements'
import {motion} from 'framer-motion'
import styles from './MetodoStripe.module.scss'

import config from '../../../../config'
import {createStripePayment, prepareStripePayment} from '../../../../actions/subscription_actions'
import Alert from '../../../../components/Alert'
import Button from '../../../../components/Button'
import Card from '../../../../components/Card'
import {Input} from '../../../../components/FormElements'


const createOptions = () => {
  return {
    style: {
      base: {
        color: '#424770',
        '::placeholder': {
          color: '#aab7c4',
        },
      },
      invalid: {
        color: '#9e2146',
      },
    },
  }
}


const MetodoStripe = () => (
  <StripeProvider apiKey={config.stripeKey}>
    <Checkout />
  </StripeProvider>
)


const Checkout = () => {
  const {membership,  isProcessed, error} = useSelector(state => ({
    membership: state.userSubscription.membership,
    isProcessed: state.userSubscription.isStripeProcessed,
    error: state.userSubscription.error,
  }))
  
  const membershipDetails = config.memberships.find(m => m.name === membership)  

  const renderError = () => {
    if (error) {
      return (
        <Alert key="props" className={styles.alert} >{error}</Alert>
      )    
    }
  }

  if (isProcessed === true) {
    return (
      <Redirect push to="/user-area" />
    )
  }

  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      <div className={styles.Heading}>
        <h2>Agrega tu tarjeta de crédito</h2>
        <p>Rellena los datos de tu tarjeta de crédito</p>
      </div>

      <div className={styles.method}>
        <Card className={styles.payCard}>
          <p className={styles.title}>Tu plan elegido</p>
          <div className={styles.subCard}>
            <div className={styles.desc}>
              <p className={styles.memtitle}>{membershipDetails.details.name}</p>
              <p className={styles.small}>{membershipDetails.details.service}</p>
              <p className={styles.small}>{membershipDetails.details.cycle}</p>
            </div>
            <div className={styles.change}>
              <Link to="/sign-up/editplan"><p className={styles.title}>Cambiar</p></Link>
            </div>
          </div>
        </Card>

        <Card className={`${styles.payCard} ${styles.creditCard}`}>
          <Elements>
            <SplitForm
              membershipDetails={membershipDetails}
            />
          </Elements>

          <p className={styles.footNote}>No almacenamos los datos de tu tarjeta</p>
        </Card>
        {renderError()}
      </div>
    </motion.div>
  )

}

const _SplitForm = props => {
  const [name, setName] = useState('')
  const dispatch = useDispatch()
  const {isLoading, stripeClientSecret, transactionId} = useSelector(state => ({
    isLoading: state.userSubscription.isStripeLoading,
    isProcessed: state.userSubscription.isStripeProcessed,
    stripeClientSecret: state.userSubscription.stripeClientSecret,
    transactionId: state.userSubscription.transactionId
  }))

  //Paso 1, Primer intento de pagar al pulsar el boton
  const handleSubmit = ev => {
    ev.preventDefault()
    if (props.stripe) {
      props.stripe.createPaymentMethod('card', {billing_details: {name}}).then(pMethod => {
        if (pMethod.paymentMethod) {
          dispatch(prepareStripePayment({membershipDetails:props.membershipDetails, pMethodId: pMethod.paymentMethod.id, cardName: name}))
        }
      })
    } else {
      console.log("Stripe.js hasn't loaded yet.")
    }
  }

  //Pago no completado, mostrar modal SCA
  useEffect(() => {
    if (stripeClientSecret !== null) {
      props.stripe.handleCardPayment(stripeClientSecret, {payment_method_data: {billing_details: {name}}}).then((intent) =>{
        dispatch(createStripePayment(
          {
            membershipDetails:props.membershipDetails, 
            intentId: intent.paymentIntent.id, 
            transactionId,
            cardName: name
          }
        ))
      })
    }
  },[dispatch, name, props.membershipDetails, props.stripe, stripeClientSecret, transactionId])


    
  return (
    <form onSubmit={handleSubmit}>
      <label className={styles.cardLabel}>
          Nombre en la tarjeta
        <Input onChange={(e) => setName(e.target.value)} placeholder="Nombre en la tarjeta" required type="text" />
      </label>

      <label className={styles.cardLabel}>
          Número de tarjeta
        <CardElement
          className={styles.StripeElement}
          hidePostalCode={true}
          {...createOptions()}
        />
      </label>

      <Button className={styles.payButton} isLoading={isLoading}>{isLoading ? 'Pagando...':'Suscribir pago'}</Button>
    </form>
  )
}
const SplitForm = injectStripe(_SplitForm)

export default MetodoStripe
