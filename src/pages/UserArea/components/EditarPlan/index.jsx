import React, {useState} from 'react'
import {useDispatch} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {motion} from 'framer-motion'
import {chooseUserSubscription} from '../../../../actions/user_actions'
import Subscriptiontable from '../../../../components/Subscriptiontable'
import styles from './EditarPlan.module.scss'
import config from '../../../../config'


const EditarPlan = () => {
  const [done, setDone] = useState(false)

  const dispatch = useDispatch()

  const setSubscription = (sub) => {
    dispatch(chooseUserSubscription(sub))
    setDone(true)
  }


  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      <div className={styles.Heading}>
        <h2>Cambia tu suscripción</h2>
        <p>El cambio se hará efectivo en el siguiente pago</p>
      </div>
      <Subscriptiontable buttonAction={setSubscription} />
      {done && <Redirect push to='/user-area/metodo-pago' />}
    </motion.div>
  )

}

export default EditarPlan
