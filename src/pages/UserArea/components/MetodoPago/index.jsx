import React, {useEffect, useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Link} from 'react-router-dom'
import {motion} from 'framer-motion'
import {createPaypalPayment} from '../../../../actions/subscription_actions'
import {chooseUserSubscription} from '../../../../actions/user_actions'
import config from '../../../../config'
import Alert from '../../../../components/Alert'
import Button from '../../../../components/Button'
import Card from '../../../../components/Card'
import styles from './MetodoPago.module.scss'


const MetodoPago = () => {
  
  const dispatch = useDispatch()
  const [preReady, setPreReady] = useState(false)
  const {error, membership, role, isPaypalLoading, formData, showForm} = useSelector(state => {
    return {
      error: state.userSubscription.error,
      membership: state.userSubscription.membership,
      role: state.user.role,
      isPaypalLoading: state.userSubscription.isPaypalLoading,
      formData: state.userSubscription.formData,
      showForm: (preReady && !state.userSubscription.isPaypalLoading) ? true : false 
    }
  })

  //Sacar los detalles se la suscripcion a partir del nombre
  const paypalImg = require('../../../../assets/img/paypal.svg')
  const creditImg = require('../../../../assets/img/visamaster.svg')


  //Efecto secundario para evitar el siguiente bug
  //Si la pagina redirige a Paypal, y el usuario vuelve
  //La página empezó un bucle infinito de redireccion
  useEffect(()=>{
    setPreReady(isPaypalLoading)
  },[isPaypalLoading])


  useEffect(()=>{
    if (!membership) {
      const myMembership = config.memberships.find(m => m.name === role)
      if (myMembership !== false) {
        dispatch(chooseUserSubscription(myMembership.name))
      }
    }
  }, [dispatch, membership, role])


  const paypalCheckout = () => {

    if (showForm && !error) {
      return (
        <FormularioPaypal formData={formData} />
      )
    }
  }

  const renderError = () => {
    if (error) {
      return (
        <Alert key="props" className={styles.alert} >{error}</Alert>
      )    
    }
  }

  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      <div className={styles.Heading}>
        <h2>Elige el método de pago</h2>
        <p>El cambio se hará efectivo en el siguiente pago</p>
      </div>

      <div className={styles.methods}>
        <Card className={styles.payCard}>
          <div className={styles.logo}><img src={paypalImg} alt="Paypal" /></div>
          <div className={styles.name}>Paypal</div>
          <div className={styles.authorise}><Button onClick={() => dispatch(createPaypalPayment(membership))} small isLoading={isPaypalLoading}>{!isPaypalLoading && 'Suscribir'}</Button></div>
        </Card>

        <Card className={styles.payCard}>
          <div className={styles.logo}><img src={creditImg} alt="Tarjetas de credito" /></div>
          <div className={styles.name}>Tarjeta de crédito</div>
          <div className={styles.authorise}><Link to="/user-area/metodo-pago/stripe"><Button small>Suscribir</Button></Link></div>
        </Card>

        {renderError()}
      </div>
      {paypalCheckout()}
    </motion.div>
  )
}

const FormularioPaypal = (props) => {
  const pForm = React.createRef()

  useEffect(() => {
    if (pForm !== null) {
      pForm.current.submit()
    }
  },[pForm])


  return (
    <React.Fragment>
      <form 
        method="POST"
        id="paypalForm" 
        action={config.paypalGateway}
        ref={pForm}
      >
        {props.formData.map((dt, i) => <input key={i} type="hidden" name={dt.name} value={dt.value} />)}
      </form>
    </React.Fragment>
  )
}


export default MetodoPago
