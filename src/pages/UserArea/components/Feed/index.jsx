import React, {useEffect, useRef} from 'react'
import moment from 'moment'
import 'moment/locale/es'
import {useSelector, useDispatch} from 'react-redux'
import {getUserFeed, setLikeUserFeed} from '../../../../actions/user_actions'
import Alert from '../../../../components/Alert'
import Card from '../../../../components/Card'
import Icon from '../../../../components/Icon'
import UserPlayer from '../../../../components/UserPlayer'
import styles from './Feed.module.scss'


const Feed = () => {
  const dispatch = useDispatch()

  const hasFeed = useRef(false)
  useEffect(() => {
    if (hasFeed.current === false) {
      dispatch(getUserFeed())
      hasFeed.current = true
    }
  }, [hasFeed, dispatch])

  const {error, isLoading, feed, beats, role, credits} = useSelector(state => {
    return {
      error: state.feed.error,
      isLoading: state.feed.isLoading,
      feed: state.feed.feed,
      beats: state.feed.beats,
      role: state.user.role,
      credits: state.user.credits
    }
  })

  const FeedCard = (f, i, type, body) =>  {
    moment.locale('es')
    const publishdate = new moment(f.publishdate*1000).fromNow()

    return (
      <Card className={styles.feedCard} key={i}>
        <div className={styles.titleRow}>
          <div className={styles.cat}>{type}</div>
          <div className={styles.date}>{publishdate}</div>
        </div>
        <div className={styles.bodyRow}>
          {body}
        </div>
        <div className={styles.footRow}>
          <span className={styles.footnote}>¿Te gustó la publicación?</span>
          <Icon 
            onClick={() => dispatch(setLikeUserFeed(f.id))}
            primaryicon="far fa-heart"
            primarystyle={`${styles.actionIcon}`}
            selected={f.liked}
            selectedicon="fas fa-heart"
            selectedstyle={`${styles.actionIcon} ${styles.selected}`}
          />
          <span className={styles.likeCount}>{f.likes}</span>
        </div>
      </Card>
    )
  }

  const AnnouncementCard = (f, i) => {
    const type="Nuevo mensaje del equipo"
    const body = <div dangerouslySetInnerHTML={{__html:f.message}}></div>
    return FeedCard(f, i, type, body)
  }

  const BeatCard = (f, i) => {
    const type="Hemos añadido un nuevo beat"
    const body = 
    <UserPlayer 
      beats={f.beat === null ? [] : [beats.find(b => b.id === f.beat)]}
      oneRowPlayer={true}
      isLoading={isLoading}
      error={error}
      role={role}
      credits={credits}
    />
    return FeedCard(f, i, type, body)
  }


  
  const renderLoading = () => (
    <div className={styles.loading}>
      <i className="fal fa-spin fa-compact-disc"></i>
    </div>
  )

  const renderError = () => (
    <Alert>Ha ocurrido un error, puedes refrescar <span onClick={() => dispatch(getUserFeed())}>aquí</span></Alert>
  )

  if (isLoading) {
    return renderLoading()
  }

  if (error) {
    return renderError()
  }

  return (
    <div className={styles.feedContainer}>
      {feed.map((f, i) => {
        if (f.feedType === 'announcement') {
          return AnnouncementCard(f, i)
        }
        if (f.feedType === 'beat') {
          return BeatCard(f, i)
        }
        return null
      })}
    </div>
  )
}
export default Feed
