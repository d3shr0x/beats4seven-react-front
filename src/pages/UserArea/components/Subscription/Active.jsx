import React, {useEffect, useState, useRef} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Link} from 'react-router-dom'
import {getUserSubscription, cancelUserSubscription} from '../../../../actions/user_actions'
import config from '../../../../config'
import Alert from '../../../../components/Alert'
import Button from '../../../../components/Button'
import Card from '../../../../components/Card'
import Overlay from '../../../../components/Overlay'
import styles from './Subscription.module.scss'
import paypalImg from '../../../../assets/img/paypal.svg'
import creditImg from '../../../../assets/img/visamaster.svg'


const Subscription = props => {
  const [cancelModal, enableCancelModal] = useState(false)
  let canceModalReadyToClose = useRef(false)

  const {error, isLoading, isLoadingCancel, paymentMethod, subscription, expiryDate, paymentAddress, isCancelled} = useSelector(state => {
    return {
      error: state.userSubscription.error,
      cancelError: state.userSubscription.cancelError,
      isLoading: state.userSubscription.isLoading,
      isLoadingCancel: state.userSubscription.isLoadingCancel,
      subscription: state.user.subscription,
      expiryDate: state.user.expiryDate,
      paymentMethod: state.userSubscription.paymentMethod,
      paymentAddress: state.userSubscription.paymentAddress,
      isCancelled: state.userSubscription.isCancelled
    }
  })

  const membershipDetails = config.memberships.find((m) => (m.id === subscription))

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getUserSubscription())
  }, [dispatch])

  useEffect(() => {
    if (isLoadingCancel === true) {
      canceModalReadyToClose.current = true
    }
  }, [isLoadingCancel])


  useEffect(() => {
    if (canceModalReadyToClose.current === true) {
      canceModalReadyToClose.current = false
      enableCancelModal(false)
    }
  }, [canceModalReadyToClose])

  if (error) {
    return (
      <Alert key="props" >{props.error}</Alert>
    )
  }

  /* 
  if (isLoading) {
    return (
      <Card hasPadding className={styles.card}>
        <i className="fal fa-spin fa-compact-disc"></i>
      </Card>
    )
  }
  */

  const cancelSubCard = () => (
    <Card hasPadding className={`${styles.card} ${styles.leavingCard}`}>
      <p className={styles.title}>Cancelar suscripción</p>
      <div className={styles.subCard}>
        <div className={styles.desc}>
          <p className={styles.small}>Tu suscripción expirará el dia de renovación. Luego podrás descargar tus beats, pero perderás todos los créditos acumulados.</p>
        </div>
        <div className={styles.spacer}></div>
        <div className={styles.change}>
          <Button onClick={() => enableCancelModal(true)} transparent className={styles.changeLink}>Cancelar suscripción</Button>
        </div>
      </div>
    </Card>    
  )

  const renderCancelModal = () => {
    if (cancelModal === false) return
    return (
      <Overlay
        onClick={() => enableCancelModal(false)}
      >
        <Card className={styles.modal}>
          <h4>Estas a punto de cancelar tu cuenta</h4>
          <p>¿Estás seguro? Está acción no es revocable. Tu suscripción se cancelará el dia {expiryDate}. Tus créditos se perderán pasada esa fecha. Seguirás teniendo acceso a tus beats.</p>
          <div className={styles.buttonBar}>
            <Button 
              primary 
              isLoading={isLoadingCancel}
              onClick={() => dispatch(cancelUserSubscription())}
            >{!isLoadingCancel && 'Sí, quiero cancelar'}</Button>
            <Button 
              dark
              onClick={() => enableCancelModal(false)}
            >No, sigo en Beats4seven</Button>
          </div>
        </Card>
      </Overlay>
    )
  }


  const cancelledSubCard = () => (
    <Card hasPadding className={`${styles.card} ${styles.leavingCard}`}>
      <p className={styles.title}>Cancelar suscripción</p>
      <div className={styles.subCard}>
        <div className={styles.desc}>
          <p className={styles.small}>Tu suscripción se cancelará el dia {expiryDate}. Usa todos tus créditos hasta esa fecha o se perderán. Seguirás teniendo acceso a tus beats.</p>
        </div>
      </div>
    </Card>    
  )

  return (
    <React.Fragment>
      <Card hasPadding className={styles.card}>
        <p className={styles.title}>Tu plan elegido</p>
        <div className={styles.subCard}>
          <div className={styles.desc}>
            <p className={styles.memtitle}>{membershipDetails.details.name}</p>
            <p className={styles.small}>{membershipDetails.details.service}</p>
            <p className={styles.small}>{membershipDetails.details.cycle}</p>
          </div>
          <div className={styles.desc}>
            <p className={styles.memtitle}>Fecha de renovación</p>
            <p className={styles.small}>{isCancelled ? 'Suscripción cancelada' : expiryDate}</p>
          </div>
          <div className={styles.spacer}></div>
          <div className={styles.change}>
            <Link to="/user-area/suscripcion"><p className={styles.changeLink}>Cambiar</p></Link>
          </div>
        </div>
      </Card> 

      <Card hasPadding className={styles.card}>
        <p className={styles.title}>Tu método de pago</p>
        <div className={`${styles.subCard} ${styles.payMethodContainer}`}>
          <div className={styles.payMethod}>
            {isLoading ? <i className="fal fa-spin fa-compact-disc"></i> : <img alt="Método de pago" src={paymentMethod === 'paypal' ? paypalImg : creditImg} />}
          </div>
          <div className={styles.desc}>
            <p className={styles.memtitle}>Información de pago</p>
            <p className={styles.small}>{isLoading ? <i className="fal fa-spin fa-compact-disc"></i> : paymentAddress}</p>
          </div>
          <div className={styles.spacer}></div>
          <div className={styles.change}>
            <Link to="/user-area/metodo-pago"><p className={styles.changeLink}>Cambiar</p></Link>
          </div>
        </div>
      </Card>       

      {isCancelled ? cancelledSubCard() : cancelSubCard()}
      {renderCancelModal()}
    </React.Fragment>
  )
}


export default Subscription
