import React from 'react'
import {useSelector} from 'react-redux'
import Active from './Active'
import Expired from './Expired'
import Soporte from '../../../../components/Soporte'
import config from '../../../../config'

const Subscription = () => {
  const {role, userid, username, displayname, email} = useSelector(state => ({
    role: state.user.role,
    userid: state.user.userid,
    username: state.user.username,
    displayname: state.user.displayname,
    email: state.user.email
  }))

  return (
    <React.Fragment>
      {role === 'expired' ? <Expired/> : ''}
      {role !== 'expired' ? <Active/> : ''}
      <Soporte 
        formid={config.forms.soporte}
        userid={userid}
        username={username}
        name={displayname}
        email={email}
      />
    </React.Fragment>
  )
}

export default Subscription