import React, {useState} from 'react'
import {useSelector,useDispatch} from 'react-redux'
import {Redirect} from 'react-router-dom'

import {chooseUserSubscription} from '../../../../actions/user_actions'
import Subscriptiontable from '../../../../components/Subscriptiontable'

import Alert from '../../../../components/Alert'
import Card from '../../../../components/Card'
import styles from './Subscription.module.scss'

const Subscription = props => {
  const [done, setDone] = useState(false)
  const dispatch = useDispatch()

  const {error, isLoading} = useSelector(state => {
    return {
      error: state.userSubscription.error,
      isLoading: state.userSubscription.isLoading,
    }
  })


  if (error) {
    return (
      <Alert key="props" >{props.error}</Alert>
    )
  }

  if (isLoading) {
    return (
      <Card hasPadding className={styles.card}>
        <i className="fal fa-spin fa-compact-disc"></i>
      </Card>
    )
  }

  const setSubscription = (sub) => {
    dispatch(chooseUserSubscription(sub))
    setDone(true)
  }


  return (
    <React.Fragment>
      {/*}
      <Card hasPadding className={styles.card}>
        <p className={styles.title}>Elige una suscripción</p>
        <p className={styles.body}>Necesitas una suscripción activa para sacarle todo el provecho a Beats4Seven, selecciona uno de los siguientes planes y vuelve al mando.</p>
      </Card> 
      {*/}

      <Subscriptiontable buttonAction={setSubscription} />
      {done && <Redirect push to='/user-area/metodo-pago' />}
    </React.Fragment>
  )
}


export default Subscription
