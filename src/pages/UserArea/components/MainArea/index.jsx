import React, {useState} from 'react'
import {useSelector} from 'react-redux'
import {motion} from 'framer-motion'
import {Tabs, Tab} from '../../../../components/Tabs'
import HeaderAlert from '../../../../components/HeaderAlert'
import Beats from '../Beats'
import Feed from '../Feed'
import Subscription from '../Subscription'
import UserDetails from '../UserDetails'
import config from '../../../../config'

const MainArea = () => {
  const [activeTab, changeTab] = useState(0)

  const {role, subChangeInProgress} = useSelector(state => ({
    role: state.user.role,
    subChangeInProgress: state.user.subChangeInProgress
  }))


  const renderTabs = () => {
    return (
      <React.Fragment>
        {role === 'expired' && <HeaderAlert title="Sus acciones se han limitado" message="Actualmente no tienes una suscripción activa, puedes descargar tus beats, pero no puedes comprar nuevos beats ni mejorar los actuales hasta que dispongas de una suscripción válida." />}
        {subChangeInProgress && <HeaderAlert title="Cambio en marcha" message="Has actualizado los datos de tu cuenta. Se actualizará en unos segundos." />}
        <Tabs active={activeTab}>
          <Tab onClick={() => changeTab(0)} index={0}>Feed</Tab>
          <Tab onClick={() => changeTab(1)} index={1}>Beats</Tab>
          <Tab onClick={() => changeTab(2)} index={2}>Suscripción</Tab>
          <Tab onClick={() => changeTab(3)} index={3}>Datos personales</Tab>
        </Tabs>
      </React.Fragment>
    )
  }

  const renderFeed = () => {
    if (activeTab === 0) {
      return <Feed/>
    }
  }

  const renderPlayer = () => {
    if (activeTab === 1) {
      return <Beats/>
    }
  }

  const renderSubscription = () => {
    if (activeTab === 2) {
      return <Subscription />
    }  
  }

  const renderUserDetails = () => {
    if (activeTab === 3) {
      return <UserDetails />
    }  
  }

  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      {renderTabs()}
      {renderFeed()}
      {renderPlayer()}
      {renderSubscription()}
      {renderUserDetails()}
    </motion.div>
  )
}

export default MainArea
