import React, {useState} from 'react'
import {PoseGroup} from 'react-pose'
import {useSelector, useDispatch} from 'react-redux'
import {setUserPassword, setUserProfile} from '../../../../actions/user_actions'
import {userProfileScheme, passFormScheme} from '../../../../schemas/perfilUsuario'
import Alert from '../../../../components/Alert'
import Button from '../../../../components/Button'
import Card from '../../../../components/Card'
import {Input} from '../../../../components/FormElements'

import styles from './UserDetails.module.scss'

const UserDetails = () => {
  const dispatch = useDispatch()

  const up = useSelector(state => ({
    infoLoading: state.userProfile.setInfoLoading,
    infoError: state.userProfile.setInfoError,
    infoErrorMessage: state.userProfile.setInfoErrorMessage,
    infoSuccess: state.userProfile.setInfoSuccess,
    pResetLoading: state.userProfile.pResetLoading,
    pResetError: state.userProfile.pResetError,
    pResetErrorMessage: state.userProfile.pResetErrorMessage,
    pResetSuccess: state.userProfile.pResetSuccess,
    firstname: state.user.firstname,
    lastname: state.user.lastname,
    username: state.user.username,
    email: state.user.email,
  }))

  const [firstname,setfirstname] = useState(up.firstname)
  const [lastname,setlastname] = useState(up.lastname)
  const [email,setemail] = useState(up.email)
  const [oldpass,setoldpass] = useState('')
  const [newpass,setnewpass] = useState('')
  const [repeatpass,setrepeatpass] = useState('')
  const [passError,updatePassError] = useState({})
  const [infoError,updateInfoError] = useState({})

  const submitInfo = async () => {
    updateInfoError(false)
    const body = {firstname, lastname, email}
    try {
      //Validacion de campos
      await userProfileScheme.validate(body, {abortEarly: false})
      //Si es valido guardalo
      dispatch(setUserProfile(body))
    } catch (err) {
      if (err.name === 'ValidationError') {
        // Mapeo de una lista de errores a
        // Un objeto de un error por key (nombre campo)
        let errors = {}
        err.inner.forEach(one => {
          errors = {...errors, [one.path]: one.message}
        })
        updateInfoError(errors)
      }
    }
  }

  const submitPassword = async () => {
    updatePassError(false)
    const body = {oldpass, newpass, repeatpass}
    try {
      //Validacion de campos
      await passFormScheme.validate(body, {abortEarly: false})
      //Si es valido guardalo
      dispatch(setUserPassword(body))
    } catch (err) {
      if (err.name === 'ValidationError') {
        // Mapeo de una lista de errores a
        // Un objeto de un error por key (nombre campo)
        let errors = {}
        err.inner.forEach(one => {
          errors = {...errors, [one.path]: one.message}
        })
        updatePassError(errors)
      }
    }
  }

  const renderInfoError = () => {
    if (up.infoError) {
      return (
        <Alert key="1">{up.infoErrorMessage}</Alert>
      )
    }
  }

  const renderPassError = () => {
    if (up.pResetError) {
      return (
        <Alert key="2">{up.pResetErrorMessage}</Alert>
      )
    }
  }

  const renderInfoSuccess = () => {
    if (up.infoSuccess) {
      return (
        <Alert key="3" success>Has actualizado tu perfil</Alert>
      )
    }
  }

  const renderPassSuccess = () => {
    if (up.pResetSuccess) {
      return (
        <Alert key="4" success>Has actualizado la contraseña</Alert>
      )
    }
  }

  return (
    <React.Fragment>
      <div className={styles.udContainer}>

        <Card hasPadding className={`${styles.card} ${styles.dataCard}`}>
          <p className={styles.title}>Información de perfil</p>

          <PoseGroup>
            {renderInfoError()}
            {renderInfoSuccess()}
          </PoseGroup>

          <form onSubmit={e => {
            e.preventDefault()
            submitInfo() 
          }}
          >
            <div className={styles.nombres}>
              <Input type="text" name="firstname" placeholder="Nombre" error={infoError.firstname} className={styles.primer} defaultValue={firstname} onChange={(e) => setfirstname(e.target.value)} />
              <Input type="text" name="lastname" placeholder="Apellidos" error={infoError.lastname} value={lastname} onChange={(e) => setlastname(e.target.value)} />
            </div>

            <Input type="text" name="username" placeholder="Nombre de usuario" disabled error={infoError.username} value={up.username} />
            <Input type="text" name="email" placeholder="Dirección de correo electrónico" error={infoError.email} groupClassName={styles.ultimo}  value={email} onChange={(e) => setemail(e.target.value)} />
            <Button isLoading={up.infoLoading}>{up.infoLoading ? 'Actualizando' : 'Actualizar información'}</Button>
          </form>
        </Card>

        <Card hasPadding className={`${styles.card} ${styles.passwordCard}`}>
          <p className={styles.title}>Cambiar contraseña</p>

          <PoseGroup>
            {renderPassError()}
            {renderPassSuccess()}
          </PoseGroup>

          <form onSubmit={e => {
            e.preventDefault()
            submitPassword() 
          }}
          >
            <Input type="password" name="old_password" placeholder="Contraseña actual" error={passError.oldpass}  value={oldpass} onChange={(e) => setoldpass(e.target.value)} />
            <Input type="password" name="password" placeholder="Contraseña nueva" error={passError.newpass}  value={newpass} onChange={(e) => setnewpass(e.target.value)} />
            <Input type="password" name="repeat_password" placeholder="Repetir contraseña" error={passError.repeatpass} groupClassName={styles.ultimo}   value={repeatpass} onChange={(e) => setrepeatpass(e.target.value)} />
            <Button isLoading={up.pResetLoading}>{up.pResetLoading ? 'Actualizando' : 'Cambiar contraseña'}</Button>
          </form>
        </Card>
      </div>
    </React.Fragment>
  )
}

export default UserDetails