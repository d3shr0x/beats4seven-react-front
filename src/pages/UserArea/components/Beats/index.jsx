import React, {useEffect, useRef} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import UserPlayer from '../../../../components/UserPlayer'
import {getUserBeats} from '../../../../actions/user_actions'


const Beats = () => {
  const dispatch = useDispatch()
  const {beats, isLoading, error, role, credits} = useSelector(state => (
    {
      beats: state.userBeats.beats,
      isLoading: state.userBeats.isLoading,
      error: state.userBeats.error,
      role: state.user.role,
      credits: state.user.credits
    }
  ))

  
  //Side-Effect que descarga los beats de la API en Redux si no hay
  const hasBeats = useRef(false)
  useEffect(() => {
    if (hasBeats.current === false) {
      dispatch(getUserBeats())
      hasBeats.current = true
    }
  }, [dispatch, hasBeats])

  return (
    <UserPlayer 
      beats={beats}
      isLoading={isLoading}
      error={error}
      role={role}
      credits={credits}
    />
  )
}
export default Beats
