import React from 'react'
import {Route, Switch} from 'react-router-dom'
import {AnimatePresence} from 'framer-motion'
import AuthRoute from '../../utils/authRoute'
import MainArea from './components/MainArea'
import EditarPlan from './components/EditarPlan'
import MetodoPago from './components/MetodoPago'
import MetodoStripe from './components/MetodoStripe'


const UserArea = () => {

  return (
    <Route
      render={({location}) => (
        <AnimatePresence>
          <Switch location={location} key={location.pathname}>
            <AuthRoute path="/user-area" exact roles={['standard', 'vip', 'expired']} component={MainArea} />
            <AuthRoute path="/user-area/suscripcion" exact roles={['standard', 'vip', 'expired']} component={EditarPlan} />
            <AuthRoute path="/user-area/metodo-pago" exact roles={['standard', 'vip', 'expired']} component={MetodoPago} />
            <AuthRoute path="/user-area/metodo-pago/stripe" exact roles={['standard', 'vip', 'expired']} component={MetodoStripe} />
          </Switch>
        </AnimatePresence>
      )}
    />
  )
}

export default UserArea
