import React, {useState} from 'react'
import {useDispatch} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {motion} from 'framer-motion'
import {setSignupSub} from '../../../../actions/signup_actions'
import Subscriptiontable from '../../../../components/Subscriptiontable'
import config from '../../../../config'
import styles from './ElegirPlan.module.scss'

const ElegirSuscripcion = () => {
  const [done, setDone] = useState(false)

  const dispatch = useDispatch()

  const setSubscription = (sub) => {
    dispatch(setSignupSub(sub))
    setDone(true)
  }

  if (done === true) {
    return <Redirect push to="/sign-up/registration" />
  }

  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit={{x:400}}
    >
      <div className={styles.Heading}>
        <p>Paso 1 de 3</p>
        <h2>Elige tu suscripción</h2>
        <p>Puedes cambiar entre planes en cualquier momento</p>
      </div>
      <Subscriptiontable buttonAction={setSubscription} />
    </motion.div>
  )
}

export default ElegirSuscripcion