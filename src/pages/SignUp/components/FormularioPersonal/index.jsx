import React, {useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {PoseGroup} from 'react-pose'
import {motion} from 'framer-motion'
import {setSignupPersonal} from '../../../../actions/signup_actions'
import {formularioRegistro} from '../../../../schemas/formularioRegistro'
import styles from './FormularioPersonal.module.scss'
import Alert from '../../../../components/Alert'
import Button from '../../../../components/Button'
import Card from '../../../../components/Card'
import {Input} from '../../../../components/FormElements'

import config from '../../../../config'

const FormularioPersonal = () => {

  const [firstname, setfirstname] = useState('')
  const [lastname, setlastname] = useState('')
  const [username, setusername] = useState('')
  const [email, setemail] = useState('')
  const [password, setpassword] = useState('')
  const [repeat_password, setrepeat_password] = useState('')
  const [error, seterror] = useState(false)

  const {isLoading, affiliateId, propsError, propsDone} = useSelector(state => ({
    isLoading: state.signup.isLoading,
    affiliateId: state.signup.affId,
    propsError: state.signup.error,
    propsDone: state.signup.done
  }))

  const dispatch = useDispatch()

  const setFormDetails = async () => {
    seterror(false)
    try {
      //Validacion de campos
      await formularioRegistro.validate({firstname,lastname,username,email,password,repeat_password}, {abortEarly: false})
      //Si es valido guardalo
      dispatch(setSignupPersonal({firstname,lastname,username,email,password,repeat_password, aff_id: affiliateId}))
    } catch (err) {
      if (err.name === 'ValidationError') {
        // Mapeo de una lista de errores a
        // Un objeto de un error por key (nombre campo)
        let errors = {}
        err.inner.forEach(one => {
          errors = {...errors, [one.path]: one.message}
        })
        seterror(errors)
      }
    }
  }

  const nextStep = () => {
    if (propsDone === true) {
      return <Redirect push to="/sign-up/payment" />
    }
  }

  const errorMessage = () => {
    if (propsError) {
      return (
        <Alert key="props" >{propsError}</Alert>
      )
    }
  }



  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      <div className={styles.Heading}>
        <p>Paso 2 de 3</p>
        <h2>Introduce los datos de tu cuenta</h2>
        <p>Con estos datos podrás identificarte en la página</p>
      </div>

      <Card hasPadding className={styles.card}>
        <PoseGroup>
          {errorMessage()}
        </PoseGroup>

        <form onSubmit={e => {
          setFormDetails(); e.preventDefault() 
        }}
        >
          <div className={styles.nombres}>
            <Input type="text" name="firstname" placeholder="Nombre" className={styles.primer} error={error.firstname}  onChange={(e) => setfirstname(e.target.value)} />
            <Input type="text" name="lastname" placeholder="Apellidos" error={error.lastname} onChange={(e) => setlastname(e.target.value)} />
          </div>
    
          <Input type="text" name="username" placeholder="Nombre de usuario" error={error.username} onChange={(e) => setusername(e.target.value)} />
          <Input type="text" name="email" placeholder="Dirección de correo electrónico" error={error.email} onChange={(e) => setemail(e.target.value)} />
          <Input type="password" name="password" placeholder="Contraseña" error={error.password} onChange={(e) => setpassword(e.target.value)} />
          <Input type="password" name="repeat_password" placeholder="Repetir contraseña" error={error.repeat_password} groupClassName={styles.ultimo}  onChange={(e) => setrepeat_password(e.target.value)} />
          <Button isLoading={isLoading}>{isLoading ? 'Creando tu cuenta' : 'Crear cuenta'}</Button>
        </form>
      </Card>

      {nextStep()}

    </motion.div>
  )

}

export default FormularioPersonal