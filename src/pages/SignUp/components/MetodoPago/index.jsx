import React, {useEffect, useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Link,Redirect} from 'react-router-dom'
import {motion} from 'framer-motion'
import ReactPixel from 'react-facebook-pixel'
import styles from './MetodoPago.module.scss'
import {createPaypalPayment} from '../../../../actions/subscription_actions'
import config from '../../../../config'
import Alert from '../../../../components/Alert'
import Button from '../../../../components/Button'
import Card from '../../../../components/Card'
import paypalImg from '../../../../assets/img/paypal.svg'
import creditImg from '../../../../assets/img/visamaster.svg'


const MetodoPago = () => {
  
  const dispatch = useDispatch()
  const [preReady, setPreReady] = useState(false)
  const {error, membership, isPaypalLoading, formData, showForm} = useSelector(state => {
    return {
      error: state.userSubscription.error,
      membership: state.signup.membership,
      isPaypalLoading: state.userSubscription.isPaypalLoading,
      formData: state.userSubscription.formData,
      showForm: (preReady && !state.userSubscription.isPaypalLoading) ? true : false 
    }
  })

  //Efecto secundario para evitar el siguiente bug
  //Si la pagina redirige a Paypal, y el usuario vuelve
  //La página empezó un bucle infinito de redireccion
  useEffect(()=>{
    setPreReady(isPaypalLoading)
  },[isPaypalLoading])


  if (!membership) {
    return <Redirect push to="/sign-up/editplan" />
  }

  const membershipDetails = config.memberships.find(m => m.name === membership)

  const paypalCheckout = () => {

    if (showForm && !error) {
      return (
        <FormularioPaypal formData={formData} />
      )
    }
  }

  const renderError = () => {
    if (error) {
      return (
        <Alert key="props" className={styles.alert} >{error}</Alert>
      )    
    }
  }

  const checkoutPaypal = () => {
    ReactPixel.track('AddPaymentInfo', {content_name: membership, value: membershipDetails.price})
    dispatch(createPaypalPayment(membership))
  }

  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      <div className={styles.Heading}>
        <p>Paso 3 de 3</p>
        <h2>Elige el método de pago</h2>
        <p>Sin permanencias <br/> Cancela en cualquier momento</p>
      </div>

      <div className={styles.methods}>
        <Card className={styles.payCard}>
          <div className={styles.logo}><img src={paypalImg} alt="Paypal" /></div>
          <div className={styles.name}>Paypal</div>
          <div className={styles.authorise}><Button onClick={() => checkoutPaypal()} small isLoading={isPaypalLoading}>{!isPaypalLoading && 'Suscribir'}</Button></div>
        </Card>

        <Card className={styles.payCard}>
          <div className={styles.logo}><img src={creditImg} alt="Tarjetas de credito" /></div>
          <div className={styles.name}>Tarjeta de crédito</div>
          <div className={styles.authorise}><Link to="/sign-up/payment/card"><Button small>Suscribir</Button></Link></div>
        </Card>

        {renderError()}

        <Card className={styles.planCard}>
          <p className={styles.title}>Tu plan elegido</p>
          <div className={styles.subCard}>
            <div className={styles.desc}>
              <p className={styles.memtitle}>{membershipDetails.details.name}</p>
              <p className={styles.small}>{membershipDetails.details.service}</p>
              <p className={styles.small}>{membershipDetails.details.cycle}</p>
            </div>
            <div className={styles.change}>
              <Link to="/sign-up/editplan"><p className={styles.title}>Cambiar</p></Link>
            </div>
          </div>
        </Card>
      </div>
      {paypalCheckout()}
    </motion.div>
  )
}

const FormularioPaypal = (props) => {
  const pForm = React.createRef()

  useEffect(() => {
    if (pForm !== null) {
      pForm.current.submit()
    }
  },[pForm])


  return (
    <React.Fragment>
      <form 
        method="POST"
        id="paypalForm" 
        action={config.paypalGateway}
        ref={pForm}
      >
        {props.formData.map((dt, i) => <input key={i} type="hidden" name={dt.name} value={dt.value} />)}
      </form>
    </React.Fragment>
  )
}


export default MetodoPago
