import React, {useState} from 'react'
import {useDispatch} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {motion} from 'framer-motion'
import {setSignupSub} from '../../../../actions/signup_actions'
import Subscriptiontable from '../../../../components/Subscriptiontable'
import config from '../../../../config'


const EditarPlan = () => {
  const [done, setDone] = useState(false)

  const dispatch = useDispatch()

  const setSubscription = (sub) => {
    dispatch(setSignupSub(sub))
    setDone(true)
  }


  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      <Subscriptiontable buttonAction={setSubscription} />
      {done && <Redirect push to='/sign-up/payment' />}
    </motion.div>
  )

}

export default EditarPlan
