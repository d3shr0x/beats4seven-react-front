import React, {useEffect,useState} from 'react'
import ReactPixel from 'react-facebook-pixel'
import {resetUserSubscription} from '../../../../actions/subscription_actions'
import {verifyOnboard} from '../../../../actions/signup_actions'
import {useSelector, useDispatch} from 'react-redux'
import {motion} from 'framer-motion'
import styles from './Completado.module.scss'
import config from '../../../../config'
import Howto from '../../../../components/Howto'
import Button from '../../../../components/Button'


const Completado = props => {
  const dispatch = useDispatch()
  const [ready, setReady] = useState(false)

  const {verifyTries, role, subscription, credits} = useSelector(state => ({
    verifyTries: state.signup.verifyTries,
    role: state.user.role,
    subscription: state.user.subscription,
    credits: state.user.credits
  }))

  useEffect(() => {
    
    dispatch(resetUserSubscription())
  }, [dispatch])

  useEffect(() => {
    dispatch(verifyOnboard())
  }, [dispatch, verifyTries])

  useEffect(() => {
    if (credits > 0 && role !== 'onboarding') {
      const membershipDetails = config.memberships.find(m => m.id === subscription)
      ReactPixel.track('Purchase', {content_name: membershipDetails.name, currency: 'USD', value: membershipDetails.price})
      setReady(true)
    }
  }, [credits, dispatch, role, subscription])

  return (
    <motion.div
      variants={config.framer.pageVariant}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      <div className={styles.Heading}>
        <p>Paso 3 de 3</p>
        <h2>Finalizando registro</h2>
        <p>Estamos completando el registro <br /> Podrás continuar al final de esta página</p>
      </div>

      <div className={styles.loading}>
        {!ready && <i className="fas fa-chevron-double-down"></i>}
        {ready &&  <Button onClick={() => props.history.push('/user-area')} >Entrar en Beats4seven</Button>}
      </div>

      <Howto />

      <div className={styles.footer}>
        <p>Estamos preparando tu cuenta, cuando se ilumine el botón podrás entrar en tu área de usuario</p>
        <Button onClick={() => props.history.push('/user-area')} className={styles.step20} isLoading={!ready}>{!ready ? 'Preparando tu cuenta...':'Entrar en Beats4seven'}</Button>
      </div>


    </motion.div>
  )
}


export default Completado
