import React from 'react'
import {Route, Switch} from 'react-router-dom'
import {AnimatePresence} from 'framer-motion'
import AuthRoute from '../../utils/authRoute'
import ElegirPlan from './components/ElegirPlan'
import EditarPlan from './components/EditarPlan'
import FormularioPersonal from './components/FormularioPersonal'
import MetodoPago from './components/MetodoPago'
import Completado from './components/Completado'
import MetodoStripe from './components/MetodoStripe'


const SignUp = () => {
  return (
    <Route
      render={({location}) => (
        <AnimatePresence>
          <Switch location={location} key={location.pathname}>
            <AuthRoute path="/sign-up" exact roles={['guest']} component={ElegirPlan} />
            <AuthRoute path="/sign-up/registration" exact roles={['guest']} component={FormularioPersonal} />
            <AuthRoute path="/sign-up/payment" exact roles={['onboarding']} component={MetodoPago} />
            <AuthRoute path="/sign-up/payment/card" exact roles={['onboarding']} component={MetodoStripe} />
            <AuthRoute path="/sign-up/editplan" exact roles={['onboarding']} component={EditarPlan} />
            <AuthRoute path="/sign-up/completed" exact roles={['onboarding','standard', 'vip', 'vip6']} component={Completado} />
          </Switch>
        </AnimatePresence>
      )}
    />
  )
}


export default SignUp
