import React, {Component} from 'react'
import Card from '../../components/Card'
import styles from './AvisoLegal.module.scss'

class AvisoLegal extends Component {

  render() {
    return (
      <Card hasPadding className={styles.static}>
        <div className={styles.container}>
          <h3><b>Aviso legal</b></h3>
          <p><b>Datos del Responsable</b></p>
          <ul>
            <li><b>Identidad del Responsable:</b>&nbsp;Rubén Cruz Hernández</li>
            <li><b>Nombre comercial:</b>&nbsp;Beats4seven</li>
            <li><b>NIF/CIF:</b>&nbsp;42207529E</li>
            <li><b>Dirección:</b>&nbsp;c. Médico Juan Sanchez. 35110 Sardina del Sur (Sardina del Sur)</li>
            <li><b>Correo electrónico:</b>&nbsp;info@beats4seven.com</li>
          </ul>
          <p>En este espacio, el usuario, podrá encontrar toda la información relativa a los términos y condiciones legales que definen las relaciones entre los usuarios y nosotros como responsables de esta web. Como usuario, es importante que conozcas estos términos antes de continuar tu navegación.</p>
          <p>Beats4seven(Rubén Cruz Hernández) como responsables de esta web, asumimos el compromiso de procesar la información de nuestros usuarios y clientes con plenas garantías y cumplir con los requisitos nacionales y europeos que regulan la recopilación y uso de los datos personales de nuestros usuarios.</p>
          <p>Esta web, por tanto, cumple rigurosamente con La Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), y con el Real Decreto 1720/2007, de 21 de diciembre, conocido como el Reglamento de desarrollo de la LOPD. Cumple también con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas (RGPD), así como con la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y Comercio Electrónico (LSSICE ó LSSI).</p>
          <p><b>condiciones generales de uso</b></p>
          <p>Las presentes Condiciones Generales regulan el uso (incluyendo el mero acceso) de las páginas de web, integrantes del sitio web de Beats4seven incluidos los contenidos y servicios puestos a disposición en ellas. Toda persona que acceda a la web, https://www.beats4seven.com (“usuario”) acepta someterse a las Condiciones Generales vigentes en cada momento del portal https://www.beats4seven.com.</p>
          <p><b>Datos personales que recabamos y cómo lo hacemos</b></p>
          <p>Leer Política de Privacidad.</p>
          <p><b>Compromisos y obligaciones de los usuarios</b></p>
          <p>El usuario queda informado, y acepta, que el acceso a la presente web no supone, en modo alguno, el inicio de una relación comercial con Beats4seven. De esta forma, el usuario se compromete a utilizar el sitio web, sus servicios y contenidos sin contravenir la legislación vigente, la buena fe y el orden público.</p>
          <p>Queda prohibido el uso de la web, con fines ilícitos o lesivos, o que, de cualquier forma, puedan causar perjuicio o impedir el normal funcionamiento del sitio web. Respecto de los contenidos de esta web, se prohíbe:</p>
          <ul>
            <li>Su reproducción, distribución o modificación, total o parcial, a menos que se cuente con la autorización de sus legítimos titulares;</li>
            <li>Cualquier vulneración de los derechos del prestador o de los legítimos titulares;</li>
            <li>Su utilización para fines comerciales o publicitarios.</li>
          </ul>
          <p>En la utilización de la web, https://www.beats4seven.com el usuario se compromete a no llevar a cabo&nbsp;<b>ninguna conducta</b>&nbsp;que pudiera dañar la imagen, los intereses y los derechos de Beats4seven o de terceros o que pudiera dañar, inutilizar o sobrecargar el portal https://www.beats4seven.com o que impidiera, de cualquier forma, la normal utilización de la web.</p>
          <p>No obstante, el usuario debe ser consciente de que las medidas de seguridad de los sistemas informáticos en Internet no son enteramente fiables y que, por tanto https://www.beats4seven.com no puede garantizar la inexistencia de malware u otros elementos que puedan producir alteraciones en los sistemas informáticos (software y hardware) del usuario o en sus documentos electrónicos y ficheros contenidos en los mismos aunque ponemos todos los medios necesarios y las medidas de seguridad oportunas para evitar la presencia de estos elementos dañinos.</p>
          <p><b>Medidas de seguridad</b></p>
          <p>Los datos personales comunicados por el usuario a Beats4seven pueden ser almacenados en bases de datos automatizadas o no, cuya titularidad corresponde en exclusiva a Rubén Cruz Hernández, asumiendo ésta todas las medidas de índole técnica, organizativa y de seguridad que garantizan la confidencialidad, integridad y calidad de la información contenida en las mismas de acuerdo con lo establecido en la normativa vigente en protección de datos.La comunicación entre los usuarios y https://www.beats4seven.com utiliza un canal seguro, y los datos transmitidos son cifrados gracias a protocolos a https, por tanto, garantizamos las mejores condiciones de seguridad para que la confidencialidad de los usuarios esté garantizada.</p>
          <p><b>Plataforma de resolución de conflictos</b></p>
          <p>Ponemos también a disposición de los usuarios la plataforma de resolución de litigios que facilita la Comisión Europea y que se encuentra disponible en el siguiente enlace:&nbsp;<a  target="_blank" rel="noopener noreferrer" href="http://ec.europa.eu/consumers/odr/">http://ec.europa.eu/consumers/odr/</a></p>
          <p><b>Derechos de propiedad intelectual e industrial</b></p>
          <p>En virtud de lo dispuesto en los artículos 8 y 32.1, párrafo segundo, de la Ley de Propiedad Intelectual, quedan expresamente prohibidas la reproducción, la distribución y la comunicación pública, incluida su modalidad de puesta a disposición, de la totalidad o parte de los contenidos de esta página web, con fines comerciales, en cualquier soporte y por cualquier medio técnico, sin la autorización de Beats4seven. El usuario se compromete a respetar los derechos de Propiedad Intelectual e Industrial titularidad de Beats4seven.</p>
          <p>El usuario conoce y acepta que la totalidad del sitio web, conteniendo sin carácter exhaustivo el texto, software, contenidos (incluyendo estructura, selección, ordenación y presentación de los mismos) podcast, fotografías, material audiovisual y gráficos, está protegida por marcas, derechos de autor y otros derechos legítimos, de acuerdo con los tratados internacionales en los que España es parte y otros derechos de propiedad y leyes de España.</p>
          <p>En el caso de que un usuario o un tercero consideren que se ha producido una violación de sus legítimos derechos de propiedad intelectual por la introducción de un determinado contenido en la web, deberá notificar dicha circunstancia a Beats4seven indicando:</p>
          <ul>
            <li>Datos personales del interesado titular de los derechos presuntamente infringidos, o indicar la representación con la que actúa en caso de que la reclamación la presente un tercero distinto del interesado.</li>
            <li>Señalar los contenidos protegidos por los derechos de propiedad intelectual y su ubicación en la web, la acreditación de los derechos de propiedad intelectual señalados y declaración expresa en la que el interesado se responsabiliza de la veracidad de las informaciones facilitadas en la notificación.</li>
          </ul>
          <p><b>Exclusión de garantías y responsable</b></p>
          <p><b>Rubén Cruz Hernández(Beats4seven) no otorga ninguna garantía ni se hace responsable, en ningún caso, de los daños y perjuicios de cualquier naturaleza que pudieran traer causa de:</b></p>
          <ul>
            <li>La falta de disponibilidad, mantenimiento y efectivo funcionamiento de la web, o de sus servicios y contenidos;</li>
            <li>La existencia de malware, programas maliciosos o lesivos en los contenidos;</li>
            <li>El uso ilícito, negligente, fraudulento o contrario a este Aviso Legal;</li>
            <li>La falta de licitud, calidad, fiabilidad, utilidad y disponibilidad de los servicios prestados por terceros y puestos a disposición de los usuarios en el sitio web.</li>
            <li>El prestador no se hace responsable bajo ningún concepto de los daños que pudieran dimanar del uso ilegal o indebido de la presente página web.</li>
          </ul>
          <p><b>Ley aplicable y jurisdicción</b></p>
          <p>Con carácter general las relaciones entre&nbsp;con los usuarios de sus servicios telemáticos, presentes en esta web se encuentran sometidas a la legislación y jurisdicción españolas y a los tribunales de Sardina del Sur.</p>
          <p><b>Contacto</b></p>
          <p>En caso de que cualquier usuario tuviese alguna duda acerca de estas Condiciones legales o cualquier comentario sobre el portal https://www.beats4seven.com, por favor diríjase a info@beats4seven.com.</p>
          <p>El aviso legal han sido actualizadas por última vez el 22-05-2018.</p>
        </div>
		 </Card>
    )
  }
}


export default AvisoLegal