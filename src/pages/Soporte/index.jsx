import React from 'react'
import Contacto from '../../components/Contacto'
import styles from 'Soporte.module.css'
import config from '../../config'

const Soporte = props => {
  return (
    <div className={styles.soporte}>
      <Contacto formid={config.forms.soporte} />
    </div>
  )
}

export  default Soporte