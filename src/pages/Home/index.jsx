import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Redirect, Link} from 'react-router-dom'
import {Link as HashLink} from "react-scroll"
import ReactPixel from 'react-facebook-pixel'

import styles from './Home.module.scss'
import {setSignupSub} from '../../actions/signup_actions'
import Footer from '../../components/Footer'
import Subscriptiontable from '../../components/Subscriptiontable'
import Button from '../../components/Button'
import Contacto from '../../components/Contacto'
import PublicPlayer from '../../components/PublicPlayer'
import CookieConsent from '../../components/CookieConsent'
import config from '../../config'

import b4sTypo from '../../assets/img/logo-typo.svg'
import producerImg from '../../assets/img/home-productores.jpg'
import producerWebp from '../../assets/img/home-productores.webp'
import beatsImg from '../../assets/img/home-beats.jpg'
import beatsWebp from '../../assets/img/home-beats.webp'


class Home extends Component {
  state = {
    signup: false
  }

  render() {
    return (
      <React.Fragment>
        <CookieConsent />
        <div className={styles.Home}>
          <div className={styles.head}>
            <div className={styles.container}>
              <div className={styles.headLine}>
                <div className={styles.logo}>
                  <img alt="logo" src={b4sTypo} />
                </div>
                <div className={styles.linkArea}>
                  <Link to="/login"><Button dark small>Login</Button></Link>
                </div>
              </div>
              <div className={styles.headContent}>
                <div className={styles.textHalf}>
                  <div className={styles.headText}>
                    <h1>MÚSICA URBANA PARA TUS CANCIONES</h1>
                    <p>Descarga beats de rap, rnb, trap, latin music, afrobeats, etc. Sin límites de uso, desde solo $7 dólares al mes</p>
                  </div>
                  <div className={styles.buttons}>
                    <HashLink smooth={true} to="beats"><Button dark>ESCUCHAR BEATS</Button></HashLink>
                    <HashLink smooth={true} to="planes"><Button>PRECIOS</Button></HashLink>
                  </div>
                </div>
                <div className={styles.imgHalf}>
                  <img src={require('../../assets/img/home-vector.svg')} alt="Ilustración cabecera Beats4seven" />
                </div>
              </div>
            </div>
          </div>
          <div id="features" className={styles.features}>
            <div className={styles.container}>
              <div className={styles.sectionHead}>
                <h2>¿Que es Beats4$even?</h2>
                <br />
                <p>Somos una plataforma que ayuda a los artistas a publicar más y mejor música. Olvida los beats exclusivos, olvida las licencias, imagina tener los beats que necesitas desde solo 7 $ al mes. Sube tus canciones a todas las plataformas digitales como Spotify, Deezer, Apple Music…etc y cancela cuando quieras, no tenemos permanencia.</p>
              </div>
              <div className={styles.featuresBody}>
                <div className={`${styles.feature} ${styles.productores}`}>
                  <div className={styles.featureText}>
                    <h2 className={styles.featureTitle}>PRODUCTORES</h2>
                    <p className={styles.featureDesc}>
                    Solo contamos con productores TOP de gran nivel y profesionalidad, que han trabajado con los mejores y ahora lo harán para ti. <br /> <br />

Nuestro equipo ha creado instrumentales para artistas como Mobb Deep, Keny Arkana, Merkules, Onyx, Zpu, Lopes, Nach, Necro, Capaz, Cappadonna, Snak the Ripper, Sean Dog Cypress Hill, El B Los Aldeanos, Eptos Uno, Hablando en Plata, Mr. Hyde… y más de 200 artistas reconocidos.  <br /> <br />

Las mejores marcas como Universal Music, Red Bull, CNN, Rtve, Chocolatex, Boa Música, Hhgroups, Mad 91 o Sfdk Records han confiado en el sonido de nuestros instrumentales, por eso cuidamos cada detalle cuando elegimos nuevos productores. <br /> <br />

                    </p>
                  </div>
                  <picture className={styles.marginLeft}>
                    <source srcSet={producerWebp} type="image/webp" />
                    <img className={styles.producerImg} src={producerImg} alt="Productores de Beats4seven" />
                  </picture>
                </div>

                <div className={`${styles.feature} ${styles.beats}`}>
                  <picture className={styles.marginRight}>
                    <source srcSet={beatsWebp} type="image/webp" />
                    <img className={styles.beatsImg} src={beatsImg} alt="Beats en Beats4seven" />
                  </picture>
                  <div className={styles.featureText}>
                    <h2 className={styles.featureTitle}>BEATS</h2>
                    <p className={styles.featureDesc}>
                      Nuestros beats no tienen límites de uso, no hay letra pequeña, olvídate de las licencias con restricciones en streams, visualizaciones, ventas...etc. <br /><br />Puedes subir tus canciones a todas las plataformas como Spotify, Itunes, Youtube... y ganar dinero con ellas, monetiza y haz lo que te de la gana. <br /><br />Si algún día te cansas de publicar tanta música, cancela cuando quieras en un solo clic. Siempre podrás seguir usando los beats descargados.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="logos" className={styles.logos}>
            <div className={styles.container}>
              <div className={styles.sectionHead}>
                <h2>CONFÍAN EN NUESTRO SONIDO</h2>
                <p>Nuestros productores trabajan con las mejores empresas musicales</p>
              </div>
              <div className={styles.logoRow1}>
                <img alt="logo" src={require('../../assets/img/logos/redbull.svg')} />
                <img alt="logo" src={require('../../assets/img/logos/cnn.svg')} />
                <img alt="logo" src={require('../../assets/img/logos/universal.svg')} />
                <img alt="logo" src={require('../../assets/img/logos/rtve.svg')} />
                <img alt="logo" src={require('../../assets/img/logos/chocolatex.svg')} />
              </div>
              <div className={styles.logoRow2}>
                <img alt="logo" src={require('../../assets/img/logos/boa.svg')} />
                <img alt="logo" src={require('../../assets/img/logos/hhgroups.svg')} />
                <img alt="logo" src={require('../../assets/img/logos/mad91.svg')} />
                <img alt="logo" src={require('../../assets/img/logos/sfdk.svg')} />
              </div>
            </div>
          </div>
          <div id="beats" className={styles.nuestrosBeats}>
            <div className={styles.container}>
              <div className={styles.sectionHead}>
                <h2>ESCUCHA NUESTROS BEATS</h2>
                <p>Consigue beats en una suscripción mensual, por 3 dólares más consigue los trackouts</p>
              </div>
              <PublicPlayer />
            </div>
          </div>
          <div id="planes" className={styles.subscribe}>
            <div className={styles.container}>
              <div className={styles.sectionHead}>
                <h2>NUESTROS PLANES</h2>
                <p>Suscríbete a un plan para poder descargar beats.</p>
              </div>
              <Subscriptiontable buttonAction={this.setSubscription} />
              {this.goToSignUp()}
            </div>
          </div>
          <div className={styles.grandesPesos}>
            <div className={styles.fila1}></div>
            <div className={styles.fila2}>
              <div className={styles.espacioIzq}></div>
              <div className={styles.promo}>
                <h3>PRODUCCIONES PARA GRANDES PESOS</h3>
                <p>Producen ahora para tí</p>
                <Link to="sign-up"><Button>Únete ahora</Button></Link>
              </div>
              <div className={styles.espacioDer}></div>
            </div>
            <div className={styles.fila3}></div>
          </div>
          <div id="planes" className={styles.subscribe}>
            <div className={styles.container}>
              <div className={styles.sectionHead}>
                <h2>¿Tienes preguntas?</h2>
                <p>Comprueba nuestras <Link to="/faq">FAQ</Link>, y si sigues con dudas, déjanos tus comentarios</p>
              </div>
              <Contacto formid={config.forms.contacto} />
            </div>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    )
  }

  setSubscription = (sub) => {
    this.props.setSignupSub(sub)
    this.setState({signup: true})
  }

  goToSignUp = () => {
    if (this.state.signup === true) {
      ReactPixel.track('Lead') 
      return <Redirect push to="/sign-up/registration" />
    }
  }
}

const mapStateToProps = ({beats}) => ({
  beats
})

export default connect(mapStateToProps, {setSignupSub})(Home)