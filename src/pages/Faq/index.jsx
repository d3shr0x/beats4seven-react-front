import React, {Component} from 'react'
import Card from '../../components/Card'
import styles from './Faq.module.scss'

class AvisoLegal extends Component {

  render() {
    return (
      <Card hasPadding className={styles.static}>
        <div className={styles.container}>
          <h2><b>FAQ</b></h2>

          <h3>¿ Qué tipo de moneda usamos ?</h3>
          <p>El precio de la suscripción a Beats4seven está en dólares estadounidenses. Si quieres saber cuanto es el cambio a tu moneda, puedes hacerlo en este enlace: https://www.xe.com/es/</p>

          <h3>¿ Cómo se realiza el pago ?</h3>
          <p>Puedes registrarte con Paypal o con tarjeta de crédito. Una vez realizas el pago, cobramos el mismo importe cada 30 días. En el plan VIP hay una oferta en la que puedes suscribirte 6 meses por 54 $.</p>

          <h3>¿ Puedo cancelar en cualquier momento ?</h3>
          <p>Si, puedes cancelar cuando quieras, además es muy fácil. En tu perfil tienes un botón para cancelar tu suscripción, haces clic ahí y confirmas, 2 clics.</p>

          <h3>¿ Qué diferencia hay entre una suscripción a Beats4seven o comprar instrumentales a un productor en su web ?</h3>
          <p>Imagina que te suscribes a nuestro plan VIP ( solo 12 $ ), con este plan puedes descargar 4 instrumentales cada mes, sin límites de uso. En la web de un productor, el tipo de licencia ilimitada cuesta unos 150 $ por un solo beat.</p>

          <p>Si usamos las matemáticas, con 150 $ podrías estar un año en Beats4seven y usar 48 instrumentales.</p> 

          <h3>¿ Puedo descargar los beats por pistas ?</h3>
          <p>Si eres usuario Vip cada instrumental que descargas te llega por pistas. Además es el plan que recomendamos por muchas razones, puedes verlas en nuestra página de inicio o en un artículo que hemos preparado en nuestro blog.</p>

          <h3>¿ Cuantos beats puedo descargar si estoy suscrito ?</h3>
          <p>Tienes acceso a todo nuestro catálogo de beats, puedes añadirlos a una lista de favoritos y descargar 4 beats cada mes, que podrás seguir usando siempre, incluso monetizando… aunque te des de baja en nuestra plataforma.</p>

          <h3>¿ Se añaden nuevos beats a la web ?</h3>
          <p>Nuestro catálogo de beats crece cada mes, cada semana, estamos en constante crecimiento.</p>

          <h3>Cuando pago la suscripción y descargo un beat, ¿ Se descarga sin marca de agua ?</h3>
          <p>Si, una vez eres usuario y descargas cualquier beat, este te llega sin marca de agua.</p>

          <h3>¿ Se pueden subir las canciones creadas a las plataformas como Spotify ?</h3>
          <p>Se pueden subir a todas las plataformas digitales, sin límites de streams. Todo el dinero que ganéis ahí es vuestro.</p>

          <h3>¿ Todo el dinero que genero con streams es para mi ?</h3>
          <p>Si, siempre. Todos los beneficios que generes con streams serán exclusivamente tuyos.</p>

          <h3>¿ Puedo monetizar las canciones con los beats que descargo ?</h3>
          <p>Si, puedes monetizar en Youtube sin límites. Además tu licencia sigue siendo válida siempre, no importa si cancelas tu cuenta con nosotros.</p>

          <h3>¿ Puedo registrar el content id de las canciones ?</h3>
          <p>No, el content id no se puede registrar ya que causaría problemas con el resto de usuarios de la plataforma y con los productores. Esto es algo normal, cuando compras licencias a productores en sus webs tampoco está permitido.</p>

          <h3>¿ Incluyen licencia de uso los beats ?</h3>
          <p>Si, cada instrumental incluye una licencia de uso, el nombre del productor del beat y los bpm. Para que esa licencia sea válida es imprescindible que el instrumental esté en tu histórico de descargas también.</p>

          <h3>¿ Qué pasa si uso un beat y luego me doy de baja en Beats4seven ?</h3>
          <p>Cuando te registras en Beats4seven, en tu perfil tienes un histórico de descargas, todos los beats que tienes en tu histórico de descargas los puedes seguir usando de por vida, aunque te des de baja.</p>

          <h3>¿ Se puede hacer algún tipo de edición en los beats ?</h3>
          <p>Puedes adaptarlo, alargando el verso por ejemplo o moviendo los estribillos, quitando algún instrumento…etc. Lo que no se puede hacer es añadir instrumentos o pistas creadas por otro productor. Pero los puedes adaptar a tu gusto.</p>

          <h3>¿ Qué es el acceso a mezclas PRO del plan VIP ?</h3>
          <p>Solo para nuestros usuarios Vip tenemos un servicio de mezcla y mastering. Contamos con un equipo de ingenieros que harán sonar tus temas con la mejor calidad profesional. Este servicio de mezcla y mastering te podría costar entre 120 y 150 $, pero nosotros lo ofrecemos solo por 40 $ y tendrás tu canción lista en menos de 72 horas. Este beneficio es solo accesible para usuarios VIP.</p>
        </div>
		 </Card>
    )
  }
}


export default AvisoLegal