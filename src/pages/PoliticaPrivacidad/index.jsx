import React, {Component} from 'react'
import Card from '../../components/Card'
import styles from './PoliticaPrivacidad.module.scss'

class PoliticaPrivacidad extends Component {

  render() {
    return (
      <Card hasPadding className={styles.static}>
        <div className={styles.container}>
          <h3>Política de privacidad</h3>
          <p>Tu privacidad es importante para nosotros. En esta declaración de privacidad se explica qué datos personales recopilamos de nuestros usuarios y cómo los utilizamos. Te animamos a leer detenidamente estos términos antes de facilitar tus datos personales en esta web. Los mayores de trece años podrán registrarse en https://www.beats4seven.com como usuarios sin el previo consentimiento de sus padres o tutores.</p>
          <p>En el caso de los menores de trece años se requiere el consentimiento de los padres o tutores para el tratamiento de sus datos personales.</p>
          <p>En ningún caso se recabarán del menor de edad datos relativos a la situación profesional, económica o a la intimidad de los otros miembros de la familia, sin el consentimiento de éstos.</p>
          <p>Si eres menor de trece años y has accedido a este sitio web sin avisar a tus padres no debes registrarte como usuario.</p>
          <p>En esta web se respetan y cuidan los datos personales de los usuarios. Como usuario debes saber que tus derechos están garantizados.</p>
          <p>Nos hemos esforzado en crear un espacio seguro y confiable y por eso queremos compartir nuestros principios respecto a tu privacidad:</p>
          <ul>
            <li>Nunca solicitamos información personal a menos que realmente sea necesaria para prestarte los servicios que nos requieras.</li>
            <li>Nunca compartimos información personal de nuestros usuarios con nadie, excepto para cumplir con la ley o en caso que contemos con tu autorización expresa.</li>
            <li>Nunca utilizaremos tus datos personales con una finalidad diferente a la expresada en esta política de privacidad.</li>
          </ul>
          <p>Es preciso advertir que esta Política de Privacidad podría variar en función de exigencias legislativas o de autorregulación, por lo que se aconseja a los usuarios que la visiten periódicamente. Será aplicable en caso de que los usuarios decidan rellenar algún formulario de cualquiera de sus formularios de contacto donde se recaben datos de carácter personal.</p>
          <p>Beats4seven ha adecuado esta web a las exigencias de la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), y al Real Decreto 1720/2007, de 21 de diciembre, conocido como el Reglamento de desarrollo de la LOPD. Cumple también con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas (RGPD), así como con la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y Comercio Electrónico (LSSICE o LSSI).</p>
          <p><b>Responsable del tratamiento de tus datos personales</b></p>
          <ul>
            <li><b>Identidad del Responsable:</b> Rubén Cruz Hernández</li>
            <li><b>Nombre comercial:</b> Beats4seven</li>
            <li><b>NIF/CIF:</b> 42207529E</li>
            <li><b>Dirección:</b> c. Médico Juan Sanchez. 35110 Sardina del Sur (Sardina del Sur)</li>
            <li><b>Correo electrónico:</b> info@beats4seven.com</li>
            <li><b>Actividad:</b> Facilitación de música digital desde internet</li>
          </ul>
          <p>A efectos de lo previsto en el Reglamento General de Protección de Datos antes citado, los datos personales que nos envíes a través de los formularios de la web, recibirán el tratamiento de datos de “Usuarios de la web y suscriptores”.</p>
          <p>Para el tratamiento de datos de nuestros usuarios, implementamos todas las medidas técnicas y organizativas de seguridad establecidas en la legislación vigente.</p>
          <p><b>Principios que aplicaremos a tu información personal</b></p>
          <p>En el tratamiento de tus datos personales, aplicaremos los siguientes principios que se ajustan a las exigencias del nuevo reglamento europeo de protección de datos:</p>
          <ul>
            <li><b>Principio de licitud, lealtad y transparencia:</b> Siempre vamos a requerir tu consentimiento para el tratamiento de tus datos personales para uno o varios fines específicos que te informaremos previamente con absoluta transparencia.</li>
            <li><b>Principio de minimización de datos:</b> Solo vamos a solicitar datos estrictamente necesarios en relación con los fines para los que los requerimos. Los mínimos posibles.</li>
            <li><b>Principio de limitación del plazo de conservación:</b> los datos serán mantenidos durante no más tiempo del necesario para los fines del tratamiento, en función a la finalidad, te informaremos del plazo de conservación correspondiente, en el caso de suscripciones, periódicamente revisaremos nuestras listas y eliminaremos aquellos registros inactivos durante un tiempo considerable.</li>
            <li><b>Principio de integridad y confidencialidad:</b> Tus datos serán tratados de tal manera que se garantice una seguridad adecuada de los datos personales y se garantice confidencialidad. Debes saber que tomamos todas las precauciones necesarias para evitar el acceso no autorizado o uso indebido de los datos de nuestros usuarios por parte de terceros.</li>
          </ul>
          <p><b>¿Cómo hemos obtenido tus datos?</b></p>
          <p>Los datos personales que tratamos en Beats4seven proceden de:</p>
          <ul>
            <li>Formulario de contacto</li>
            <li>Formulario de subscripción</li>
            <li>Formulario de solicitud de servicios</li>
            <li>Formulario de registro</li>
          </ul>
          <p><b>¿Cuáles son tus derechos cuando nos facilitas tus datos?</b></p>
          <p>Cualquier persona tiene derecho a obtener confirmación sobre si en Beats4seven estamos tratando datos personales que me concierne, o no.</p>
          <p>Las personas interesadas tienen derecho a:</p>
          <ul>
            <li>Solicitar el acceso a los datos personales relativos al interesado</li>
            <li>Solicitar su rectificación o supresión</li>
            <li>Solicitar la limitación de su tratamiento</li>
            <li>Oponerse al tratamiento</li>
            <li>Solicitar la portabilidad de los datos</li>
          </ul>
          <p>Los interesados podrán <b>acceder</b> a sus datos personales, así como a solicitar la <b>rectificación</b> de los datos inexactos o, en su caso, solicitar su <b>supresión</b> cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. En determinadas circunstancias, los interesados podrán solicitar la <b>limitación</b> del tratamiento de sus datos, en cuyo caso únicamente los conservaré para el ejercicio o la defensa de reclamaciones.</p>
          <p>En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán <b>oponerse</b> al tratamiento de sus datos.Rubén Cruz Hernández dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones. Cómo interesado, tienes derecho a recibir los datos personales que te incumban, que nos hayas facilitado y en un formato estructurado, de uso común y lectura mecánica, y a transmitirlos a otro responsable del tratamiento cuando:</p>
          <ul>
            <li>El tratamiento esté basado en el consentimiento</li>
            <li>Los datos hayan sido facilitados por la persona interesada.</li>
            <li>El tratamiento se efectúe por medios automatizados.</li>
          </ul>
          <p>Al ejercer tu derecho a la portabilidad de los datos, tendrás derecho a que los datos personales se transmitan directamente de responsable a responsable cuando sea técnicamente posible.</p>
          <p>Los interesados también tendrán derecho a la tutela judicial efectiva y a presentar una reclamación ante la autoridad de control, en este caso, la Agencia Española de Protección de Datos, si consideran que el tratamiento de datos personales que le conciernen infringe el Reglamento.</p>
          <p><b>¿Con qué finalidad tratamos tus datos personales?</b></p>
          <p>Cuando un usuario se conecta con esta web por ejemplo para comentar un post, mandar un correo al titular, suscribirse o realizar alguna contratación, está facilitando información de carácter personal de la que es responsable Beats4seven. Esa información puede incluir datos de carácter personal como pueden ser tu dirección IP, nombre, dirección física, dirección de correo electrónico, número de teléfono, y otra información. Al facilitar esta información, el usuario da su consentimiento para que su información sea recopilada, utilizada, gestionada y almacenada por https://www.beats4seven.com, sólo como se describe en el Aviso Legal y en la presente Política de Privacidad.</p>
          <p>En Beats4seven existen diferentes sistemas de captura de información personal y trato la información que nos facilitan las personas interesadas con el siguiente fin por cada sistema de captura (formularios):</p>
          <ul>
            <li><b>Formulario de contacto:</b> Solicitamos los siguientes datos personales: Nombre, Email, para responder a los requerimientos de los usuarios de https://www.beats4seven.com. Por ejemplo, podemos utilizar esos datos para responder a tu solicitud y dar respuesta a las dudas, quejas, comentarios o inquietudes que puedas tener relativas a la información incluida en la web, los servicios que se prestan a través de la web, el tratamiento de sus datos personales, cuestiones referentes a los textos legales incluidos en la web, así como cualesquiera otras consultas que puedas tener y que no estén sujetas a las condiciones de contratación. Te informo que los datos que nos facilitas estarán ubicados en los servidores de:
              <ul>
                <li>Cdmon (proveedor de Rubén Cruz Hernández) fuera de la UE en Otros. Cdmon está ubicado en UE, un país cuyos nivel de protección son adecuados según Comisión de la UE.</li>
              </ul>
            </li>
            <li> Te informo que los datos que nos facilitas estarán ubicados en los servidores de:
              <ul>
                <li><b>Formulario de suscripción a contenidos:</b> En este caso, solicitamos los siguientes datos personales: Nombre, Email, apellido, para gestionar la lista de suscripciones, enviar boletines, promociones y ofertas especiales, facilitados por el usuario al realizar la suscripción. Dentro de la web existen varios formularios para activar la suscripción. Los boletines electrónicos o newsletter están gestionados por Cdmon.Cdmon (proveedor de Rubén Cruz Hernández) fuera de la UE en Otros. Cdmon está ubicado en UE, un país cuyos nivel de protección son adecuados según Comisión de la UE.</li>
              </ul>
            </li>
            <li><b>Formulario de solicitud de servicios:</b> Solicitamos los siguientes datos personales: Nombre, Email, para solicitar alguno de los servicios que https://www.beats4seven.com pone a disposición de sus usuarios. La información recabada permitirá solicitar el servicio correspondiente para una posible tramitación del mismo offline. Las solicitudes serán respondidas por correo electrónico. La web no permite gestionar pagos directamente. Te informo que los datos que nos facilitas estarán ubicados en los servidores de:
              <ul>
                <li>Cdmon (proveedor de Rubén Cruz Hernández) fuera de la UE en Otros. Cdmon está ubicado en UE, un país cuyos nivel de protección son adecuados según Comisión de la UE.</li>
              </ul>
            </li>
            <li><b>Formulario de Registro:</b> En este caso, solicitamos los siguientes datos personales: Nombre, Email, apellido, para que puedas crearte una cuenta en nuestro sistema y tengas acceso a sus funcionalidades. Te informo que los datos que nos facilitas estarán ubicados en los servidores de:
              <ul>
                <li>Cdmon (proveedor de Rubén Cruz Hernández) fuera de la UE en Otros. Cdmon está ubicado en UE, un país cuyos nivel de protección son adecuados según Comisión de la UE.</li>
              </ul>
            </li>
          </ul>
          <p>Existen otras finalidades por la que tratamos tus datos personales:</p>
          <ul>
            <li>Para garantizar el cumplimiento de las condiciones de uso y la ley aplicable. Esto puede incluir el desarrollo de herramientas y algoritmos que ayudan a esta web a garantizar la confidencialidad de los datos personales que recoge.</li>
            <li>Para apoyar y mejorar los servicios que ofrece esta web.</li>
            <li>También se recogen otros datos no identificativos que se obtienen mediante algunas cookies que se descargan en el ordenador del usuario cuando navega en esta web que detallo en la política de cookies.</li>
            <li>Para gestionar las redes sociales. Beats4seven puede tener presencia en redes sociales. El tratamiento de los datos que se lleve a cabo de las personas que se hagan seguidoras en las redes sociales de las páginas oficiales de https://www.beats4seven.com, se regirá por este apartado. Así como por aquellas condiciones de uso, políticas de privacidad y normativas de acceso que pertenezcan a la red social que proceda en cada caso y aceptadas previamente por el usuario de Rubén Cruz Hernández. Tratará sus datos con las finalidades de administrar correctamente su presencia en la red social, informando de actividades, productos o servicios de Beats4seven. Así como para cualquier otra finalidad que las normativas de las redes sociales permitan. En ningún caso utilizaremos los perfiles de seguidores en redes sociales para enviar publicidad de manera individual.</li>
          </ul>
          <p>De acuerdo a lo establecido en el reglamento general de protección de datos europeo (RGPD) 2016/679, Rubén Cruz Hernández (Beats4seven) con domicilio en c. Médico Juan Sanchez. 35110 Sardina del Sur (Sardina del Sur) , Sardina del Sur, 35110 será responsable del tratamiento de los datos correspondientes a Usuarios de la web y suscriptores.</p>
          <p>Beats4seven, no vende, alquila ni cede datos de carácter personal que puedan identificar al usuario, ni lo hará en el futuro, a terceros sin el consentimiento previo. Sin embargo, en algunos casos se pueden realizar colaboraciones con otros profesionales, en esos casos, se requerirá consentimiento a los usuarios informando sobre la identidad del colaborador y la finalidad de la colaboración. Siempre se realizará con los más estrictos estándares de seguridad.</p>
          <p><b>Legitimación para el tratamiento de tus datos</b></p>
          <p>La base legal para el tratamiento de sus datos es: el consentimiento.</p>
          <p>Para contactar o realizar comentarios en esta web se requiere el consentimiento con esta política de privacidad.</p>
          <p>La oferta prospectiva o comercial de productos y servicios está basada en el consentimiento que se le solicita, sin que en ningún caso la retirada de este consentimiento condicione la ejecución del contrato de suscripción.</p>
          <p><b>Categoría de datos</b></p>
          <p>Las categorías de datos que se tratan son datos identificativos.</p>
          <p>No se tratan categorías de datos especialmente protegidos.</p>
          <p><b>¿Por cuánto tiempo conservaremos tus datos?</b></p>
          <p><b>Los datos personales proporcionados se conservarán:</b></p>
          <ul>
            <li>Hasta que no se solicite su supresión por el interesado.</li>
          </ul>
          <p><b>¿A qué destinatarios se comunicarán tus datos?</b></p>
          <p>Muchas herramientas que utilizamos para gestionar tus datos son contratados por terceros.</p>
          <p>Para prestar servicios estrictamente necesarios para el desarrollo de la actividad, https://www.beats4seven.com, comparte datos con los siguientes prestadores bajo sus correspondientes condiciones de privacidad:</p>
          <p><b>Google Analytics:</b> un servicio analítico de web prestado por Google, Inc., una compañía de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos (“Google”). Google Analytics utiliza “cookies”, que son archivos de texto ubicados en tu ordenador, para ayudar a https://www.beats4seven.com a analizar el uso que hacen los usuarios del sitio web. La información que genera la cookie acerca de su uso de https://www.beats4seven.com (incluyendo tu dirección IP) será directamente transmitida y archivada por Google en los servidores de Estados Unidos.</p>
          <p><b>Hosting:</b> Cdmon, con domicilio en UE. Más información en: www.cdmon.com (Cdmon). Cdmon trata los datos con la finalidad de realizar sus servicios de hosting a Rubén Cruz Hernández.</p>
          <p><b>Email marketing:</b> AWeber Systems, Inc., con domicilio en EEUU. Más información en: https://www.aweber.com (AWeber Systems, Inc.). AWeber Systems, Inc. trata los datos con la finalidad de realizar sus servicios de email marketing a Rubén Cruz Hernández.</p>
          <p><b>Navegación</b></p>
          <p>Al navegar por https://www.beats4seven.com se pueden recoger datos no identificables, que pueden incluir, direcciones IP, ubicación geográfica (aproximadamente), un registro de cómo se utilizan los servicios y sitios, y otros datos que no pueden ser utilizados para identificar al usuario. Entre los datos no identificativos están también los relacionados a tus hábitos de navegación a través de servicios de terceros. Esta web utiliza los siguientes servicios de análisis de terceros:</p>
          <ul>
            <li>Metricool</li>
            <li>Google analytics</li>
            <li>Pixel de Facebook</li>
          </ul>
          <p>Utilizamos esta información para analizar tendencias, administrar el sitio, rastrear los movimientos de los usuarios alrededor del sitio y para recopilar información demográfica sobre nuestra base de usuarios en su conjunto.</p>
          <p><b>Secreto y seguridad de los datos</b></p>
          <p>https://www.beats4seven.com se compromete en el uso y tratamiento de los datos incluidos personales de los usuarios, respetando su confidencialidad y a utilizarlos de acuerdo con la finalidad del mismo, así como a dar cumplimiento a su obligación de guardarlos y adaptar todas las medidas para evitar la alteración, pérdida, tratamiento o acceso no autorizado, de conformidad con lo establecido en la normativa vigente de protección de datos.</p>
          <p>Esta web incluye un certificado SSL. Se trata de un protocolo de seguridad que hace que tus datos viajen de manera íntegra y segura, es decir, la transmisión de los datos entre un servidor y usuario web, y en retroalimentación, es totalmente cifrada o encriptada.</p>
          <p>https://www.beats4seven.com no puede garantizar la absoluta inexpugnabilidad de la red Internet y por tanto la violación de los datos mediante accesos fraudulentos a ellos por parte de terceros.</p>
          <p>Con respecto a la confidencialidad del procesamiento, Rubén Cruz Hernández se asegurará de que cualquier persona que esté autorizada por Beats4seven para procesar los datos del cliente (incluido su personal, colaboradores y prestadores), estará bajo la obligación apropiada de confidencialidad (ya sea un deber contractual o legal).</p>
          <p>Cuando se presente algún incidente de seguridad, al darse cuenta Beats4seven, deberá notificar al Cliente sin demoras indebidas y deberá proporcionar información oportuna relacionada con el Incidente de Seguridad tal como se conozca o cuando el Cliente lo solicite razonablemente.</p>
          <p><b>Exactitud y veracidad de los datos</b></p>
          <p>Como usuario, eres el único responsable de la veracidad y corrección de los datos que remitas a https://www.beats4seven.com exonerando a Rubén Cruz Hernández (Beats4seven), de cualquier responsabilidad al respecto.</p>
          <p>Los usuarios garantizan y responden, en cualquier caso, de la exactitud, vigencia y autenticidad de los datos personales facilitados, y se comprometen a mantenerlos debidamente actualizados. El usuario acepta proporcionar información completa y correcta en el formulario de contacto o suscripción.</p>
          <p><b>Aceptación y consentimiento</b></p>
          <p>El usuario declara haber sido informado de las condiciones sobre protección de datos de carácter personal, aceptando y consintiendo el tratamiento de los mismos por parte de Rubén Cruz Hernández (Beats4seven) en la forma y para las finalidades indicadas en esta política de privacidad.</p>
          <p><b>Revocabilidad</b></p>
          <p>El consentimiento prestado, tanto para el tratamiento como para la cesión de los datos de los interesados, es revocable en cualquier momento comunicándolo a Rubén Cruz Hernández (Beats4seven) en los términos establecidos en esta Política para el ejercicio de los derechos ARCO. Esta revocación en ningún caso tendrá carácter retroactivo.</p>
          <p><b>Cambios en la política de privacidad</b></p>
          <p>Rubén Cruz Hernández se reserva el derecho a modificar la presente política para adaptarla a novedades legislativas o jurisprudenciales, así como a prácticas de la industria. En dichos supuestos, Rubén Cruz Hernández anunciará en esta página los cambios introducidos con razonable antelación a su puesta en práctica.</p>
          <p><b>Correos comerciales</b></p>
          <p>De acuerdo con la LSSICE, https://www.beats4seven.com no realiza prácticas de SPAM, por lo que no envía correos comerciales por vía electrónica que no hayan sido previamente solicitados o autorizados por el usuario. En consecuencia, en cada uno de los formularios habidos en la web, el usuario tiene la posibilidad de dar su consentimiento expreso para recibir el boletín, con independencia de la información comercial puntualmente solicitada.</p>
          <p>Conforme a lo dispuesto en la Ley 34/2002 de Servicios de la Sociedad de la Información y de comercio electrónico, https://www.beats4seven.com se compromete a no enviar comunicaciones de carácter comercial sin identificarlas debidamente.</p>
          <p>Documento revisado el 22-05-2018.</p>
        </div>
      </Card>
    )
  }
}


export default PoliticaPrivacidad