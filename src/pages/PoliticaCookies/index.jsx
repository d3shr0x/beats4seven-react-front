import React, {Component} from 'react'
import Card from '../../components/Card'
import styles from './PoliticaCookies.module.scss'

class PoliticaCookies extends Component {

  render() {
    return (
      <Card hasPadding className={styles.static}>
        <div className={styles.container}>
          <h3>Política de cookies</h3>
          <p>En esta web recopilamos y utilizamos la información según indicamos en nuestra política de privacidad. Una de las formas en las que recopilamos información es a través del uso de la tecnología llamada “cookies”. En Beats4seven , utilizamos cookies para varias cosas.</p>
          <p><b>¿Qué es una cookie?</b></p>
          <p>Una “cookie” es una pequeña cantidad de texto que se almacena en tu navegador (como Chrome de Google o Safari de Apple) cuando navegas por la mayoría de los sitios web.</p>
          <p><b>¿Qué NO es una cookie?</b></p>
          <p>No es un virus, ni un troyano, ni un gusano, ni spam, ni spyware, ni abre ventanas pop-up.</p>
          <p><b>¿Qué información almacena una cookie?</b></p>
          <p>Las cookies no suelen almacenar información sensible sobre usted, como tarjetas de crédito o datos bancarios, fotografías o información personal, etc. Los datos que guardan son de carácter técnico, estadísticos, preferencias personales, personalización de contenidos, etc.</p>
          <p>El servidor web no le asocia a usted como persona sino a su navegador web. De hecho, si usted navega habitualmente con el navegador Chrome y prueba a navegar por la misma web con el navegador Firefox, verá que la web no se da cuenta que es usted la misma persona porque en realidad está asociando la información al navegador, no a la persona.</p>
          <p><b>¿Qué tipo de cookies existen?</b></p>
          <ul>
            <li><b></b><b>Cookies técnicas:</b>&nbsp;Son las más elementales y permiten, entre otras cosas, saber cuándo está navegando un humano o una aplicación automatizada, cuándo navega un usuario anónimo y uno registrado, tareas básicas para el funcionamiento de cualquier web dinámica.</li>
            <li><b></b><b>Cookies de análisis:</b>&nbsp;Recogen información sobre el tipo de navegación que está realizando, las secciones que más utiliza, productos consultados, franja horaria de uso, idioma, etc.</li>
            <li><b></b><b>Cookies publicitarias:</b>&nbsp;Muestran publicidad en función de su navegación, su país de procedencia, idioma, etc.</li>
          </ul>
          <p><b>¿Qué son las cookies propias y las de terceros?</b></p>
          <p>Las cookies propias son las generadas por la página que está visitando y las de terceros son las generadas por servicios o proveedores externos como Mailchimp, Mailrelay, Facebook, Twitter, Google adsense, etc.</p>
          <p><b>¿Qué cookies utiliza esta web?</b></p>
          <p>Esta web utiliza cookies propias y de terceros. En este sitio web se utilizan las siguientes cookies que se detallan a continuación:</p>
          <p><b>Cookies propias:</b></p>
          <p><b>Inicio de sesión:</b>&nbsp;Las cookies para iniciar sesión te permiten entrar y salir de tu cuenta de Beats4seven.</p>
          <p><b>Personalización:</b>&nbsp;Las cookies nos ayudan a recordar con qué personas o sitios web has interactuado, para que podamos mostrarte contenido relacionado.</p>
          <p><b>Preferencias:</b>&nbsp;Las cookies nos permite recordar tus ajustes y preferencias, como el idioma preferido y tu configuración de privacidad.</p>
          <p><b>Seguridad:</b>&nbsp;Utilizamos cookies para evitarte riesgos de seguridad. Principalmente para detectar cuándo alguien esta intentando piratear tu cuenta de Beats4seven.</p>
          <p><b>Cookies de terceros</b></p>
          <p>Esta web utiliza servicios de análisis, concretamente, Google Analytics para ayudar al website a analizar el uso que hacen los usuarios del sitio web y mejorar la usabilidad del mismo, pero en ningún caso se asocian a datos que pudieran llegar a identificar al usuario. Google Analytics, es un servicio analítico de web prestado por Google, Inc., El usuario puede consultar&nbsp;<a  href="https://www.google.es/intl/es/policies/technologies/types/" target="_blank" rel="noopener noreferrer" >aquí</a>&nbsp;el tipo de cookies utilizadas por Google.</p>
          <p>Rubén Cruz Hernández es usuario de la plataforma de suministro y alojamiento de blogs&nbsp;<b>WordPress</b>, propiedad de la empresa norteamericana Automattic, Inc. A tales efectos, los usos de tales cookies por los sistemas nunca están bajo control o gestión del responsable de la web, pueden cambiar su función en cualquier momento, y entrar cookies nuevas. Estas cookies tampoco reportan al responsable de esta web beneficio alguno. Automattic, Inc., utiliza además otras cookies con la finalidad de ayudar a identificar y rastrear a los visitantes de los sitios de&nbsp;<b>WordPress</b>, conocer el uso que hacen del sitio web de Automattic, así como sus preferencias de acceso al mismo, tal y como se recoge en el apartado “Cookies” de su política de privacidad.</p>
          <p>Las cookies de redes sociales pueden almacenarse en su navegador mientras navega por https://www.beats4seven.com por ejemplo, cuando utiliza el botón de compartir contenidos de https://www.beats4seven.com en alguna red social.</p>
          <p>A continuación tienes información sobre las cookies de las redes sociales que utiliza esta web en sus propias políticas de cookies:</p>
          <ul>
            <li>Cookies de Facebook, ver más información en su&nbsp;<a  href="https://www.facebook.com/policies/cookies/" target="_blank" rel="noopener noreferrer" >política de cookies</a></li>
            <li>Cookies de Instagram, ver más información en su&nbsp;<a  href="https://help.instagram.com/1896641480634370" target="_blank" rel="noopener noreferrer" >política de cookies</a></li>
            <li>Cookies de Youtube, ver más información en su&nbsp;<a  href="http://www.google.com/intl/es/policies/technologies/types/" target="_blank" rel="noopener noreferrer" >política de cookies</a></li>
          </ul>
          <p><b>¿Se pueden eliminar las cookies?</b></p>
          <p>Sí, y no sólo eliminar, también bloquear, de forma general o particular para un dominio específico.</p>
          <p>Para eliminar las cookies de un sitio web debe ir a la configuración de su navegador y allí podrá buscar las asociadas al dominio en cuestión y proceder a su eliminación.</p>
          <ul>
            <li>Configuración de cookies para&nbsp;<a  href="http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647" target="_blank" rel="noopener noreferrer" >Google Chrome</a></li>
            <li>Configuración de cookies para&nbsp;<a  href="https://support.apple.com/es-es/HT201265" target="_blank" rel="noopener noreferrer" >Apple Safari</a></li>
            <li>Configuración de cookies para&nbsp;<a  href="https://support.microsoft.com/en-us/help/17442/windows-internet-explorer-delete-manage-cookies#ie=ie-10" target="_blank" rel="noopener noreferrer" >Internet Explorer</a></li>
            <li>Configuración de cookies para&nbsp;<a  href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&amp;redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank" rel="noopener noreferrer" >Mozilla Firefox</a></li>
          </ul>
          <p><b>Más información sobre las cookies</b></p>
          <p>Puedes consultar el reglamento sobre cookies publicado por la Agencia Española de Protección de Datos en su “Guía sobre el uso de las cookies” y obtener más información sobre las cookies en Internet,&nbsp;<a  href="http://www.aboutcookies.org/" target="_blank" rel="noopener noreferrer" >http://www.aboutcookies.org/</a></p>
          <p>Si desea tener un mayor control sobre la instalación de cookies, puede instalar programas o complementos a su navegador, conocidos como herramientas de “Do Not Track”, que le permitirán escoger aquellas cookies que desea permitir.</p>
          <p>Esta política de cookies ha sido actualizada por última vez el 22-05-2018.</p>
		      </div>
      </Card>
    )
  }
}


export default PoliticaCookies