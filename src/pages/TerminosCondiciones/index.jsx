import React, {Component} from 'react'
import Card from '../../components/Card'
import styles from './TerminosCondiciones.module.scss'

class TerminosCondiciones extends Component {

  render() {
    return (
      <Card hasPadding className={styles.static}>
        <div className={styles.container}>
          <h3>TÉRMINOS Y CONDICIONES DE LA PLATAFORMA BEATS4SEVEN</h3>
          <p><b>¿ CÓMO FUNCIONA BEATS4SEVEN ?</b></p>
          <p>
            Puedes ser miembro de Beats4seven a través de dos planes de suscripción mensual: <br />
            1. <b>Plan estandar</b>, tiene un precio de 7 $ mensuales e incluye:<br />
                - 4 licencias de beats al mes, que se descargan con créditos ( Cada mes se añaden 4 créditos al hacer el pago ).<br />
                - Posibilidad de comprar créditos extra.<br />
            2. <b>Plan Vip</b>, con un precio de 12 $ mensuales e incluye:<br />
                - 4 licencias de beats al mes + las pistas de los cuatro beats. Que se descargan con créditos ( Cada mes se añaden 4 créditos al hacer el pago ).<br />
                - Posibilidad de comprar créditos extra. <br />
            También disponemos de un tercer plan VIP, una oferta en la que te puedes registrar 6 meses en la plataforma por solo 54 $, con un único pago, sin suscripción. <br />
          </p>
          <br />
          <p><b>CONDICIONES GENERALES</b></p>
          <p>
            - Al inscribirte en Beats4seven no tienes ningún período de permanencia, puedes darte de baja al mes siguiente sin ningún problema y sin tener que pagar otra cuota. Te puedes dar de baja desde tu perfil. En el botón de cancelar cuenta haces clic y aceptas, así de fácil.<br />
            - En el momento de suscribirte te asignamos 4 créditos, esos créditos serán tu moneda para bajar los beats, cada beat cuesta 1 crédito.<br />
            - Si no gastas tus créditos mensuales, se acumulan para el mes siguiente, por lo que un mes puedes descargar 2 beats y al mes siguiente 6 beats, eres libre de gastarlos como y cuando quieras.<br />
            - Puedes comprar créditos extra si así lo necesitas. Cada crédito extra tiene un precio de 7 $.<br />
            - No ofrecemos instrumentales exclusivas, aunque te podemos poner en contacto con muy buenos productores que pueden crear algo personalizado para ti. <br />
            - Nuestras licencias las puedes utilizar como quieras, donde quieras, sin límite de ventas, sin límites de usos comerciales, sin límites de streams o plays, puedes monetizar en Youtube... etc. <br />
            -  No está permitido registrar el content id de las canciones que publiques. Esto afectaría a otros usuarios de la plataforma y nos veríamos obligados a eliminar tu tema en todas las publicaciones que hayas hecho.<br />
            -  No se puede volver a vender una licencia adquirida en Beats4seven.<br />
            -  Solo puede utilizar los beats el usuario registrado en la plataforma, no puedes registrarte tú y descargar beats para un amigo, está todo registrado con content id y se bloquearía el tema en cualquier publicación, además causarás baja en la plataforma. Si sois un grupo de música de varios miembros y avisáis no hay problema. <br />
            - En cualquier publicación hay que nombrar ( Music by “ nombre del productor ” adquirido en Beats4seven ). <br />
            - En caso de que el pago de la mensualidad no se haga efectiva por cualquier circunstancia, o haya un incumplimiento de las cláusulas del contrato por parte del usuario, Beats4seven se reserva el derecho a dar de baja al usuario. <br />
            - Beats4seven se reserva el derecho de actualización de la totalidad o de alguno de los términos de uso del contrato. Las nuevas versiones del contrato de términos de uso se considerarán vigentes en el momento que se publiquen en el portal para los nuevos usuarios registrados y 15 días después para los usuarios registrados con anterioridad a su publicación. <br />
            - Beats4seven se reserva el derecho de suspensión total o parcial de los servicios que presta en el portal. La suspensión total o parcial de la prestación del servicio supondrá la cancelación del contrato de términos de uso y venta de publicaciones acordado con el usuario. La suspensión total del servicio de Beats4seven supondría la suspensión de todos y cado uno de los servicios que se prestan al usuario. <br />
          </p>
          <br />
          <p><b>COMPRAS DE CRÉDITOS EXTRA</b></p>
          <p>
            - En Beats4seven te damos la posibilidad de comprar créditos extra una vez agotas los tuyos y no quieres esperar al mes siguiente. Cada crédito cuesta 7 $. <br />
          </p>
          <br />
          <p><b>ALTA EN LA PLATAFORMA Y PAGOS</b></p>
          <p>
            - El usuario deberá manifestar que ha leído y acepta los términos de uso del presente contrato marcando la correspondiente casilla. En el caso de que el usuario no marcara la casilla de aceptación, el alta del usuario en el registro de usuarios no se producirá hasta que no se realice correctamente. El registro de alta en el portal y el uso de la plataforma está reservado a las personas que pueden firmar y contratar en los términos que establezca la legislación española de compras por internet. Si usted es menor de 18 años deberá estar autorizado por sus padres o tutores para registrarse en el portal. <br />
            - Una vez hayas seleccionado tu plan de suscripción, procederás al pago via Paypal o tarjeta de crédito, el pago es 100% seguro. <br />
            - Al siguiente mes se te cobrará automáticamente la nueva mensualidad, en caso de no querer seguir siendo miembro, puedes darte de baja desde tu perfil de usuario. Seguirás activo hasta que finalize el mes. <br />
            - Cuando se produzca el alta en el registro de usuarios generarás un identificador personal y password. El IDs y password son estrictamente personales y confidenciales y no deben ser revelados a terceras personas. <br />
            - En el caso de que Beats4seven compruebe que un usuario hace un uso indebido de sus claves de registro se reserva el derecho de proceder a la baja definitiva del usuario en el registro general de usuarios. <br />
            - Los datos de registro que comunique el usuario se entiende que son ciertos y pertenecen a la persona que realiza el alta en el registro de usuarios. En el caso de que se produzca una modificación en los datos de registro proporcionados, el usuario se compromete a modificarlos en el portal en el menor plazo posible. <br />
            - Los precios de las publicaciones y de los servicios de Beats4seven serán perfectamente visibles y accesibles para los usuarios. Los precios se indicarán en dólares estadounidenses, incluyendo las tasas e impuestos como el I.V.A. <br />
          </p>
          <br />
          <p><b>USO DE LA PLATAFORMA</b></p>

          <p>
            - Los archivos que el usuario descargue en la plataforma de Beats4seven, para su posterior uso en publicaciones u obras musicales propias, son responsabilidad exclusiva del usuario. Beats4seven manifiesta que no asume responsabilidad alguna sobre los contenidos de la publicación ni respecto de las posibles reclamaciones de terceras personas. <br />
            - Beats4seven se reserva el derecho de insertar anuncios publicitarios en la plataforma que serán visibles para todos los usuarios que accedan a ella. <br />
            -  La no disponibilidad de la prestación del servicio de HOSTING durante el período de vigencia del contrato cuando esta no disponibilidad sea debida a problemas en la red Internet, causas de caso fortuito o fuerza mayor o cualquier otra contingencia imprevisible ajena a la buena fe de la empresa. En todo caso, Beats4seven se compromete a tratar de solucionar los problemas que puedan surgir y a ofrecer el apoyo necesario al usuario de forma que se solucionen estas posibles incidencias de la forma más rápida y satisfactoria posible. Asimismo Beats4seven se reserva el derecho a interrumpir temporalmente la prestación del servicio por necesidades imperativas de mejora o reparación de los medios técnicos utilizados para la prestación del mismo.<br />
            -  Las suscripciones tendrán un período mínimo de un mes. Pueden ser canceladas por los usuarios directamente en su perfil, en caso de no ser cancelada por el usuario, se seguirá cobrando cada 30 días.<br />
            - Los cobros se realizan de forma automática cada 30 dias, si en el momento de hacer el cobro da error, se intentará otra vez a los 3 dias, luego a los 5 y si este último vuelve a dar error, el usuario causa baja en la plataforma.<br />
            - Beats4seven no realiza devoluciones de ningún tipo. Somos transparentes con nuestros usuarios y arreglaremos cualquier problema de manera amistosa.<br />
          </p>
        </div> 
      </Card>
    )
  }
}


export default TerminosCondiciones