import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {PoseGroup} from 'react-pose'
import {userSignin, resetUser} from '../../actions/auth_actions'
import {resetPassword} from '../../actions/public_actions'
import Alert from '../../components/Alert'
import Button from '../../components/Button'
import {Input} from '../../components/FormElements'

import styles from './Login.module.scss'

class Login extends Component {
  state = {
    isLoginPage: true,
    username:"",
    password:"",
    error: null
  }

  componentDidMount = () => {
    this.props.resetUser()
  }

  resetState = () => {
    this.setState({error: null})
    this.props.resetUser()
  }

  onUserSignin = () => {
    //Resetting any error message
    this.resetState()
    
    if (this.state.username === "" || this.state.password === "") {
      this.setState({error: "Por favor rellena los campos"})
    } else {
      let userDets = {
        username: this.state.username,
        password: this.state.password
      }
      this.props.userSignin(userDets)
    }
  }

  onPasswordReset = () => {
    //Resetting any error message
    this.resetState()

    if (this.state.username === "") {
      this.setState({error: "Por favor rellena los campos"})
    } else {
      this.props.resetPassword(this.state.username)
    }
  }

  errorMessage = () => {
    if (this.state.error) {
      return (
        <Alert key="state" >{this.state.error}</Alert>
      )
    }

    if (this.props.error) {
      return (
        <Alert key="props" >{this.props.error}</Alert>
      )    
    }
  }


  render() {
    return (
      <div className={styles.card}>
        {this.state.isLoginPage ? this.loginContent() : this.resetPassContent()}
        <div className={styles.adCard}>
          <h2>¡Bienvenido a Beats4seven!<br /> tus beats están aquí</h2>
          <p>Estás a punto de navegar por nuestro catálogo enorme de instrumentales, están organizados por categorías ( Hip hop, trap, Rnb, Latin music, afrobeats, pop…etc ) para que tengas más fácil encontrar lo que buscas. Cada semana añadimos nuevos beats, desde lo más clásico hasta lo más actual.</p>
          <br />
          <p>Si tienes cualquier duda puedes visitar nuestra página de preguntas frecuentes haciendo clic <Link className={styles.link} to="/faq">aquí</Link> </p>
        </div>
      </div>
    )
  }

  loginContent() {
    const {isLoading} = this.props
    return (
      <div className={styles.loginCard}>

        <PoseGroup>
          {this.errorMessage()}
        </PoseGroup>

        <p className={styles.titleText}>Bienvenido de nuevo</p>
        <form className={styles.signForm} onSubmit={e => {
          this.onUserSignin(); e.preventDefault() 
        }}
        >
          <Input className={styles.step20} type="text" placeholder="Nombre de usuario"  onChange={(e) => this.setState({username: e.target.value})} />
          <Input className={styles.step20} type="password" placeholder="●●●●●●●●"  onChange={(e) => this.setState({password: e.target.value})}  />
          <Button className={styles.step20} isLoading={isLoading}>{isLoading ? 'Conectando...':'Conéctate'}</Button>
        </form>

        <Button transparent className={styles.step40}>
          <span onClick={() => {
            this.resetState()
            this.setState({isLoginPage:false})
          }} className={styles.noPassText}
          >Olvidé mi contraseña</span>
        </Button>

        <p className={styles.titleText}>¿No estás registrado?</p>
        <Link to="/sign-up"><Button dark className={styles.signup}>Créate una cuenta</Button></Link>
      </div>
    )
  }

  resetPassContent() {
    const {isLoading} = this.props
    return (
      <div className={styles.loginCard}>

        <PoseGroup>
          {this.errorMessage()}
        </PoseGroup>

        <PoseGroup>
          {this.props.passwordResetSuccess && <Alert key="passwordReset" success>Has recibido un correo electrónico con instrucciones para recuperar la contraseña.</Alert>}
        </PoseGroup>


        <p className={styles.titleText}>Restablece tu contraseña</p>
        <form className={styles.signForm} onSubmit={e => {
          this.onPasswordReset(); e.preventDefault() 
        }}
        >
          <Input type="text" placeholder="Nombre de usuario o correo electrónico"  onChange={(e) => this.setState({username: e.target.value})} />
          <Button className={styles.step20} isLoading={isLoading}>{isLoading ? 'Restableciendo...':'Restablecer'}</Button>
        </form>

        <Button transparent className={styles.step40}>
          <span onClick={() => {
            this.resetState()
            this.setState({isLoginPage:true})
          }} className={styles.noPassText}
          >Volver al login</span>
        </Button>

      </div>
    )
  }
}

const mapStateToProps = ({user, userLogin}) => ({
  error: userLogin.error,
  token: user.token,
  passwordResetSuccess: userLogin.passwordResetSuccess,
  isLoading: userLogin.loading
})

export default connect(mapStateToProps, {userSignin, resetUser, resetPassword})(Login)
