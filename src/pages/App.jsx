import React, {useEffect, useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Route, Switch, Redirect, useLocation, useHistory} from 'react-router-dom'
import posed, {PoseGroup} from 'react-pose'
import queryString from 'query-string'
import semver from 'semver'
import {hotjar} from 'react-hotjar'
import ReactPixel from 'react-facebook-pixel'

import {healthCheck,clearStore} from '../actions/app_actions'
import {getUserDetails} from '../actions/user_actions'
import {setSignupAffid} from '../actions/signup_actions'
import AuthRoute from '../utils/authRoute'
import Footer from '../components/Footer'
import Header from '../components/Header'
import CookieConsent from '../components/CookieConsent'
import Home from './Home'
import Login from './Login'
import SignUp from './SignUp'
import AvisoLegal from './AvisoLegal'
import PoliticaCookies from './PoliticaCookies'
import PoliticaPrivacidad from './PoliticaPrivacidad'
import TerminosCondiciones from './TerminosCondiciones'
import UserArea from './UserArea'
import AdminArea from './AdminArea'
import AffiliateArea from './AffiliateArea'
import Faq from './Faq'
import Howto from './Howto'
import config from '../config'

import styles from './App.module.scss'

const RouteContainer = posed.div({
  enter: {opacity: 1, delay: 200, beforeChildren: true},
  exit: {opacity: 0}
})

const App = () => {
  const dispatch = useDispatch()
  const location = useLocation()
  const history = useHistory()
  const {appVersion, tokenHasExpired, noInternet, cookieConsent, role} = useSelector(state => ({
    appVersion: state.app.appVersion,
    tokenHasExpired: state.app.tokenHasExpired,
    noInternet: state.app.noInternet,
    cookieConsent: state.visitor.cookieConsent,
    role: state.user.role
  }))
  const [expired, isExpired] = useState(false)

  //Lanzamos un log de bienvenida con la version de la App
  useEffect(()=>{
    console.info(`Beats4seven Frontend v${config.appVersion}`)
  },[])

  //Inicializar Pixel de Facebook
  useEffect(()=>{
    const options = {
      autoConfig: true, // set pixel's autoConfig
      debug: false, // enable logs
    }
    ReactPixel.init(config.facebookPixel, {}, options)
    if (!cookieConsent) {
      ReactPixel.revokeConsent()
    }
  },[cookieConsent])

  //Consentimiento de cookies
  useEffect(()=>{
    if (cookieConsent) {
      ReactPixel.grantConsent()
      ReactPixel.pageView()
      hotjar.initialize(config.hotjarId, 6)
    }
  },[cookieConsent])

  //Comprueba que el back-end esté activo,
  //Y si tenemos la ultima version del front
  useEffect(()=>{
    dispatch(healthCheck())
    //Recibe la última información del usuario y refresca el token
    if (role !== 'guest') {
      dispatch(getUserDetails())
    }
  },[dispatch, role])


  //Si el token ha expirado tenemos que limpiar el store y redirigir a la portada
  useEffect(()=>{
    if (tokenHasExpired) {
      dispatch(clearStore())
      isExpired(true)
    }
  },[dispatch, tokenHasExpired])


  //La API envia constantemente la version del front
  //Comparar contra la config
  useEffect(()=>{
    //En un futuro quiero una advertencia pidiendo al usuario que actualice
    if (semver.gt(appVersion, config.appVersion)) {
      //window.location.reload(true)
      console.info('You have an outdated version. Restart')
    }
  },[appVersion]) 

  //Si no hay conexión a internet, muestra una advertencia
  useEffect(()=>{
    if (noInternet) {
      //TODO: En un futuro debe activar una bandera para mostrar la advertencia
    }
  },[noInternet])

  //Si en la url hay una ID de referencia, guardar en el store
  useEffect(() => {
    const query = queryString.parse(location.search)
    if (!isNaN(query.wpam_id)) {
      dispatch(setSignupAffid(parseInt(query.wpam_id)))
      history.push({pathname: location.pathname})
    }
  },[dispatch, history, location, location.pathname, location.search])

  if (expired) {
    return <Redirect push to="/login" />
  }

  if (location.pathname === '/') {
    return (
      <React.Fragment>
        <Switch>
          <AuthRoute path="/" exact roles={['guest', 'onboarding']} component={Home} />
        </Switch>
      </React.Fragment>
    )

  }

  return (
    <Route
      render={({location}) => (
        <div>
          <CookieConsent />
          <div className={styles.background}>
            <div className={styles.container}>
              <Header />
              <PoseGroup>
                <RouteContainer key={location.pathname}>
                  <Switch location={location}>
                    <AuthRoute path="/login" exact roles={['guest']} component={Login} />
                    <Route path="/sign-up" component={SignUp} />
                    <AuthRoute path="/user-area" roles={['standard', 'vip', 'expired']} component={UserArea} />
                    <AuthRoute path="/affiliate-area" roles={['affiliate']} component={AffiliateArea} />
                    <AuthRoute path="/admin-area" roles={['admin']} component={AdminArea} />
                    <Route path="/affiliate-area-0" exact component={AffiliateArea} />
                    <Route path="/terminos-condiciones" exact component={TerminosCondiciones} />
                    <Route path="/politica-privacidad" exact component={PoliticaPrivacidad} />
                    <Route path="/aviso-legal" exact component={AvisoLegal} />
                    <Route path="/politica-cookies" exact component={PoliticaCookies} />
                    <Route path="/faq" exact component={Faq} />
                    <Route path="/how-it-works" exact component={Howto} />
                    <Redirect to="/" />
                  </Switch>
                </RouteContainer>
              </PoseGroup>
            </div>
          </div>
          <Footer />
        </div>
      )}
    />
  )
}

export default App
