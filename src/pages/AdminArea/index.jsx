import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {getAffiliateSubs} from '../../actions/affiliate_actions'
import Card from '../../components/Card'
import {Select} from '../../components/FormElements'
import styles from './AdminArea.module.scss'

const AdminArea = () => {

  //Date switcher
  const [currentDate, selectDate] = useState(0)
  const dispatch = useDispatch()

  //Redux Store
  const {displayname, loading, error, beneficios, suscriptores, listaTransacciones, listaMeses} = useSelector(state => ({
    displayname: state.user.displayname,
    loading: state.affiliate.loading,
    error: state.affiliate.error,
    beneficios: state.affiliate.beneficios,
    suscriptores: state.affiliate.suscriptores,
    listaTransacciones: state.affiliate.listaTransacciones,
    listaMeses: state.affiliate.listaMeses
  }))

  const calendario = [
    'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
  ]

  //Side Effect to fetch data
  useEffect(() => {
    dispatch(getAffiliateSubs(currentDate))
  },[currentDate, dispatch])

  const updateMonthSelector = (i) => {
    selectDate(i)
  }

  if (error || !listaMeses) {
    return (
      <Card hasPadding className={styles.card}>
        Error al cargar, actualiza la página
      </Card>
    )
  }

  if (loading || !listaMeses[currentDate]) {
    return (
      <Card hasPadding className={styles.card}>
        <i className="fal fa-spin fa-compact-disc"></i>
      </Card>
    )
  }

  return (
    <Card hasPadding className={styles.card}>
      <div className={styles.resumen}>
        <div className={styles.bienvenida}>
          <h2>Hola {displayname}</h2>
        </div>
        <div className={styles.beneficios}>
          <h4>Beneficios {calendario[Number(listaMeses[currentDate].month)-1]} {listaMeses[currentDate].year}</h4>
          <p>{beneficios}$</p>
        </div>
        <div className={styles.suscriptores}>
          <h4>Suscriptores nuevos {calendario[Number(listaMeses[currentDate].month)-1]} {listaMeses[currentDate].year}</h4>
          <p>{suscriptores}</p>
        </div>
      </div>
      <div className={styles.half}>
        <div className={styles.dateForm}>
          <span>Fecha</span>
          <Select 
            onChange={e => {
              updateMonthSelector(e.target.value)
            }}
            groupClassName={styles.monthSelector}
            value={currentDate}
          >
            {listaMeses.map((mes, i) => (
              <option key={i} value={i}>{`${calendario[Number(mes.month)-1]} ${mes.year}`}</option>
            ))}
          </Select>
        </div>
        <div className={styles.table}>
          <table>
            <thead>
              <tr>
                <th>Usuario</th>
                <th>Fecha registro</th>
                <th>Recurrente (Si/No)</th>
                <th>Beneficio</th>
              </tr>
            </thead>
            <tbody>
              {listaTransacciones.map((tran, i) => (
                <tr key={i}>
                  <td>{tran.user_id}</td>
                  <td>{tran.created_at}</td>
                  <td>{tran.recurrent ? 'Si' : 'No'}</td>
                  <td>{tran.price}$</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </Card>
  )
}

export default AdminArea
